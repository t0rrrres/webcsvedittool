/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.servlet.ServletContext;

public class LoggerUtility {
  
  static public Logger getLogger(String classname) {
    Logger logger = Logger.getLogger(classname);
    // set filter to ensure that class name and method name is fetched
    logger.setFilter(LoggingInferCallerFilter.getTheFilter());
    return logger;
  }
  
static public void setHandler(ServletContext app,Logger _logger) throws IOException{
      String log_file_path = app.getInitParameter(LogicNames.LOCAL_PATH_ACTION_LOGING_CONTEXT_PARAM_NAME);
      String log_file_name = app.getInitParameter(LogicNames.ACTIONLOG_FILENAME_CONTEXT_PARAM_NAME);
      String realPath = app.getRealPath(log_file_path)+log_file_name;
      Logger logger  = _logger;
      FileHandler fh = new FileHandler(realPath + ".log", true);
      fh.setFormatter(new SimpleFormatter());
      logger.addHandler(fh); 
  }

static public void closeHandler(Logger logger){
    
    for(Handler h:logger.getHandlers())
        {
            h.close();   //must call h.close or a .LCK file will remain.
        }
}
} // class
