/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;

import javax.servlet.http.*;

/**
 *
 * @author dieter
 */

// wss 20191111
public class ServletUtilityFunctions {

  static public String getFilenameLecturers(HttpServletRequest request) {
    String res = null;
    String filename = request.getServletContext().getInitParameter(LogicNames.LECTURERS_FILENAME_CONTEXT_PARAM_NAME);
    if (filename != null)  {
      res = filename;
    } else {
      filename = Consts.DEFAULT_LECTURERS_FILENAME;
    }
    return res;
  }
  
  static public String getFilenameRooms(HttpServletRequest request) {
    String res = null;
    String filename = request.getServletContext().getInitParameter(LogicNames.ROOMS_FILENAME_CONTEXT_PARAM_NAME);
    if (filename != null)  {
      res = filename;
    } else {
      filename = Consts.DEFAULT_ROOMS_FILENAME;
    }
    return res;
  }
  
  static public String getFilenameSubjects(HttpServletRequest request) {
    String res = null;
    String filename = request.getServletContext().getInitParameter(LogicNames.SUBJECTS_FILENAME_CONTEXT_PARAM_NAME);
    if (filename != null)  {
      res = filename;
    } else {
      filename = Consts.DEFAULT_SUBJECTS_FILENAME;
    }
    return res;
  }
  
  static public String getFilenameStudyGroups(HttpServletRequest request) {
    String res = null;
    String filename = request.getServletContext().getInitParameter(LogicNames.STUDYGROUPS_FILENAME_CONTEXT_PARAM_NAME);
    if (filename != null)  {
      res = filename;
    } else {
      filename = Consts.DEFAULT_STUDYGROUPS_FILENAME;
    }
    return res;
  }

}
