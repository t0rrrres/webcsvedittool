package de.hscoburg.nedev.stdplanconfig.Models;

import java.io.Serializable;

public class Lecturer implements Serializable{
	
	private String abbr;
	private String firstName;
	private String lastName;
	private String type;
        private String comment;
	
	public Lecturer() {};
	
	public Lecturer(String abbr, String lastName,String firstName, String type,String _comment) {
		this.abbr = abbr;
		this.lastName = lastName;
		this.firstName = firstName;
		this.type = type;
                this.comment = _comment;
		
	}
	

	public void setAbbr(String abbr) {
		this.abbr = abbr;
		
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
		
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
		
	}
	
	public void SetType(String type) {
		this.type = type;
		
	}
        public void setComment(String _comment){
            this.comment = _comment;
        }
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	
	public String getAbbr() {
		return abbr;
	}
	
	public String getType() {
		return type;
		
	}
        public String getComment(){
            return comment;
        }
	
        @Override
	public String toString() {
		return abbr + "\t"+ lastName + "\t" + firstName + "\t" + type + "\t" + comment;
	}
	
	
	

	
}
