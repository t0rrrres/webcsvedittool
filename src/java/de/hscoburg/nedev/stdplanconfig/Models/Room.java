/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig.Models;

/**
 *
 * @author nesh
 */
public class Room {
    private String name;
    private String size;
    private String owner;
    private String type;
    private String comment;
    
    public Room(){};

    public Room(String name, String size, String owner, String type, String comment) {
        this.name = name;
        this.size = size;
        this.owner = owner;
        this.type = type;
        this.comment = comment;
    }
    
    

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public String getOwner() {
        return owner;
    }

    public String getType() {
        return type;
    }

    public String getComment() {
        return comment;
    }
    
    @Override
    public String toString(){
        return name + " " + size + " " + owner + " " +type +" " + comment;
    }
}
