/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig.Models;

/**
 *
 * @author nesh
 */
public class StudyGroup {
    private String basename;
    private String semester;
    private String comment;

    public StudyGroup(String _basename, String _semester, String _comment) {
        this.basename = _basename;
        this.semester = _semester;
        this.comment = _comment;
    }

    
    public String getBasename() {
        return basename;
    }

    public String getSemester() {
        return semester;
    }
    
    public String getComment() {
        return comment;
    }

    public void setBasename(String _basename) {
        this.basename = _basename;
    }

    public void setSemester(String _semester) {
        this.semester = _semester;
    }
    public void setComment(String _comment) {
        this.comment = _comment;
    }
    
    
         
    @Override
    public String toString(){
        return(basename + " " + semester + " " + comment );
    }
    
}
