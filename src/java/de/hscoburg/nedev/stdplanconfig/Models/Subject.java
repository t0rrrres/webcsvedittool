/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig.Models;

/**
 *
 * @author nesh
 */
public class Subject {
        private String shortcut;
        private String studygroupbase;
        private String studygroupsemester;
        private String longname;
        private String curriculumType;
        private String didacticType;
        private String comment;

    public Subject(String shortcut, String studygroupbase, String studygroupsemester, String longname, String curriculumType, String didacticType, String comment) {
        this.shortcut = shortcut;
        this.studygroupbase = studygroupbase;
        this.studygroupsemester = studygroupsemester;
        this.longname = longname;
        this.curriculumType = curriculumType;
        this.didacticType = didacticType;
        this.comment = comment;
    }


        
    
    public String getShortcut() {
        return shortcut;
    }

    public String getStudygroupbase() {
        return studygroupbase;
    }

    public String getStudygroupsemester() {
        return studygroupsemester;
    }

    public String getLongname() {
        return longname;
    }

    public String getCurriculumType() {
        return curriculumType;
    }

    public String getDidacticType() {
        return didacticType;
    }

    public String getComment() {
        return comment;
    }

    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }

    public void setStudygroupbase(String studygroupbase) {
        this.studygroupbase = studygroupbase;
    }

    public void setStudygroupsemester(String studygroupsemester) {
        this.studygroupsemester = studygroupsemester;
    }

    public void setLongname(String longname) {
        this.longname = longname;
    }

    public void setCurriculumType(String curriculumType) {
        this.curriculumType = curriculumType;
    }

    public void setDidacticType(String didacticType) {
        this.didacticType = didacticType;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
        
        @Override
    public String toString(){
        return (shortcut + " " +studygroupbase+" "+studygroupsemester+" "+longname+" "+curriculumType+" "+didacticType+" "+comment );
    }
        
}
