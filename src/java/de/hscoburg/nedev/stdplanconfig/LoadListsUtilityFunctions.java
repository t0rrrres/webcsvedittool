package de.hscoburg.nedev.stdplanconfig;

import de.hscoburg.nedev.stdplanconfig.Models.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LoadListsUtilityFunctions {
    
    private static Logger sLogger;
    
    public LoadListsUtilityFunctions(){
        //sLogger = LoggerUtility.getLogger(this.getClass().getName());   
        sLogger = Logger.getLogger(this.getClass().getPackage().getName() + ".custom" ); 
    }
       
    public MixedData getLecturerList(String config_file_path, String year, String filename) {
	List<Lecturer> records = new ArrayList<>();
        MixedData md ;
        // File fileDir = new File(config_file_path + year + File.separator + filename);  // original code line, should be used later after apdating stdplan
        // wss 20200126: to get stdplanconfig to work with  stdplan the value of year
        // will be temporarily ignored since  stdplan does not know years.
        File fileDir = buildFileDir(config_file_path, year, filename);
        String logtext;
        String errorRows = "";

        if(!fileDir.exists()){
           return new MixedData(null, "");
        }
	try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF-8"))) {
	    String line = br.readLine();
            while ( (line != null) ) {
		Lecturer lec;
		if(!(line.trim().startsWith("#")) && !(line.isEmpty()) ) {
		            		
                    String[] parts = line.split(";");
                    if(parts.length <=3){
                        logtext = " line=" + line + " not enough fields separated by semicolons. Line ignored.\n";
                        // errorRows stores invalid datarows
                        errorRows += line + "\n";
                        sLogger.log(Level.WARNING, logtext);
                        line = br.readLine();
                        continue;
                    }else{
                        // in some rows there are less columns. In this case I set the comment column manually
                       if(parts.length==4){
                            lec = new Lecturer(
                                "".equals(parts[0].trim())?" ":parts[0],
                                "".equals(parts[1].trim())?" ":parts[1],
                                "".equals(parts[2].trim())?" ":parts[2],
                                "".equals(parts[3].trim())?" ":parts[3],
                                " ");
                        }else{
                            lec = new Lecturer(
                                "".equals(parts[0].trim())?" ":parts[0],
                                "".equals(parts[1].trim())?" ":parts[1],
                                "".equals(parts[2].trim())?" ":parts[2],
                                "".equals(parts[3].trim())?" ":parts[3],
                                "".equals(parts[4].trim())?" ":parts[4]); 
                       }
                        records.add(lec);	  
                    }             
                }
		line = br.readLine();
            }    
	}catch(FileNotFoundException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
	}catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
	}     
        md = new MixedData(records, errorRows);
        return md;
	}
    
    public MixedData getRoomsList(String path, String year, String filename){
        
	List<Room> rooms = new ArrayList<>();
        MixedData md ;
        String logtext;
        String errorRows = "";
        File fileDir = buildFileDir(path, year, filename);
        
        if(!fileDir.exists()){
           return new MixedData(null, "");
        }
        
	try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF-8")); ) {
            	
	    String line = br.readLine();
            while ( (line != null) ) {
		Room room;
		if(!(line.trim().startsWith("#")) && !(line.isEmpty()) ) {
		            		
                    String[] parts = line.split(";");
                    if(parts.length <=3){
                        logtext=" line="+line+ " not enough fields separated by semicolons. Line ignored.";
                        errorRows += line +"\n";
                        sLogger.log(Level.WARNING, logtext);
                        line = br.readLine();
                        continue;
                    }else{
                        // in some rows there are less columns. In this case I set the comment column manually
                        if(parts.length==4){
                            room = new Room(
                                "".equals(parts[0].trim()) ? " " : parts[0],
                                "".equals(parts[1].trim()) ? " " : parts[1],
                                "".equals(parts[2].trim()) ? " " : parts[2],
                                "".equals(parts[3].trim()) ? " " : parts[3],
                                " "
                                );
                        }else{
                            room = new Room(
                                "".equals(parts[0].trim()) ? " " : parts[0],
                                "".equals(parts[1].trim()) ? " " : parts[1],
                                "".equals(parts[2].trim()) ? " " : parts[2],
                                "".equals(parts[3].trim()) ? " " : parts[3],
                                "".equals(parts[4].trim()) ? " " : parts[4]); 
                        }
                        rooms.add(room);	               
                    }
                }
		line = br.readLine();
            }
	}catch(FileNotFoundException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
	}catch(IOException e) {
	    sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());   
	}
        md = new MixedData(rooms, errorRows);
        return md;
    }
    
    public MixedData getSubjectsList(String path, String year, String filename){
        List<Subject> subjects = new ArrayList<>();
        String logtext;
        String errorRows = "";
        File fileDir = buildFileDir(path, year, filename);
        if(!fileDir.exists()){
           return new MixedData(null, "");
        }
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF-8"));) {

            String line = br.readLine();
            while ( (line != null) ) {
                Subject subject;
                if(!(line.trim().startsWith("#")) && !(line.isEmpty()) ) {

                    String[] parts = line.split(";"); 

                    if(parts.length<6 || parts.length>7){
                        logtext=" line="+line+ " not enough fields separated by semicolons. Line ignored.";
                        errorRows += line +"\n";
                        sLogger.log(Level.WARNING, logtext);
                        line = br.readLine();
                        continue;
                    }else{
                        
                        if(parts.length==6){
                            subject = new Subject(
                                    "".equals(parts[0].trim())?" ":parts[0],
                                    "".equals(parts[1].trim())?" ":parts[1],
                                    "".equals(parts[2].trim())?" ":parts[2],
                                    "".equals(parts[3].trim())?" ":parts[3],
                                    "".equals(parts[4].trim())?" ":parts[4],
                                    "".equals(parts[5].trim())?" ":parts[5],
                                    " "     
                                    );  
                        }else{
                            subject = new Subject(
                                "".equals(parts[0].trim())?" ":parts[0],
                                "".equals(parts[1].trim())?" ":parts[1],
                                "".equals(parts[2].trim())?" ":parts[2],
                                "".equals(parts[3].trim())?" ":parts[3],
                                "".equals(parts[4].trim())?" ":parts[4],
                                "".equals(parts[5].trim())?" ":parts[5],
                                "".equals(parts[6].trim())?" ":parts[6]
                        );
                        }
                        subjects.add(subject);
                    }    	               
                }
                line = br.readLine();
            }
        }catch(FileNotFoundException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());   		
        }catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());  	
        }		
	
        return new MixedData(subjects, errorRows);
    }
    
        public MixedData getStudyGroupList(String path, String year, String filename){
	List<StudyGroup> studyGroups = new ArrayList<>();
        MixedData md ;
        String logtext;
        String errorRows = "";
        File fileDir = buildFileDir(path, year, filename);
        
        if(!fileDir.exists()){
           return new MixedData(null, "");
        }

	try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF-8"));) {
           	
	    String line = br.readLine();
            while ( (line != null) ) {
		StudyGroup studyGroup;
		if(!(line.trim().startsWith("#")) && !(line.isEmpty()) ) {
		            		
                    String[] parts = line.split(";");
                     // in some rows there are less columns. In this case I set the comment column manually
                    if(parts.length<1){
                        logtext=" line="+line+ " not enough fields separated by semicolons. Line ignored.";
                        errorRows += line +"\n";
                        sLogger.log(Level.WARNING, logtext);
                        line = br.readLine();
                        continue;
                    }else{
                        
                        if(parts.length==2){
                            studyGroup = new StudyGroup(
                                    "".equals(parts[0].trim()) ? " " : parts[0],
                                    "".equals(parts[1].trim()) ? " " : parts[1],
                                    "  ");
                        }else{
                            studyGroup = new StudyGroup(  
                                    "".equals(parts[0].trim()) ? " " : parts[0],
                                    "".equals(parts[1].trim()) ? " " : parts[1],
                                    "".equals(parts[2].trim()) ? " " : parts[2]
                            ); 
                        }   
                        studyGroups.add(studyGroup);
                    }   	               
                }
		line = br.readLine();
            }
	}catch(FileNotFoundException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
	}catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
	}
	md = new MixedData(studyGroups, errorRows);
        return md;
    }
    
    public void writeToLecturerFile(String path, String year, List<Lecturer> liste, String filename){
        Lecturer lec;
        File fileDir = buildFileDir(path, year, filename);

        try(Writer fstream = new OutputStreamWriter(new FileOutputStream(fileDir), StandardCharsets.UTF_8);) {
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<liste.size();i++) {
                lec = liste.get(i);
             
                sb.append(lec.getAbbr()).append(";")
                        .append(lec.getLastName()).append(";")
                        .append(lec.getFirstName()).append(";")
                        .append(lec.getType()).append(";")
                        .append(lec.getComment()).append(";")
                        .append("\n");
            }
            fstream.write(sb.toString());
            fstream.close();
        }catch(FileNotFoundException e){
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
        }catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());    
        }
    }
    
    public void writeToRoomFile(String path,String year, List<Room> liste, String filename) {
        Room room;
        // File fileDir = new File(path + year + File.separator + "rooms");
        File fileDir = buildFileDir(path, year, filename);
        
        try(Writer fstream = new OutputStreamWriter(new FileOutputStream(fileDir), StandardCharsets.UTF_8);) {    
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<liste.size();i++) {
                room = liste.get(i);
                /*
                line+=  room.getName()+ ";" + 
                        room.getSize()+";" +
                        room.getOwner()+";"+
                        room.getType() + ";" + 
                        room.getComment() +  "\n";
                */
                sb.append(room.getName()).append(";")
                        .append(room.getSize()).append(";")
                        .append(room.getOwner()).append(";")
                        .append(room.getType()).append(";")
                        .append(room.getComment()).append(";")
                        .append("\n");
            }
            fstream.write(sb.toString());
            fstream.close();
        }catch(FileNotFoundException e){
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
        }catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());    
        }
    }
    
        public void writeToStudyGroupFile(String path, String year, List<StudyGroup> liste, String filename) {     
        StudyGroup studyGroup;
       // File fileDir = new File(path + year + File.separator + "studygroups");
        File fileDir = buildFileDir(path, year, filename);
        try(Writer fstream = new OutputStreamWriter(new FileOutputStream(fileDir), StandardCharsets.UTF_8);) {
            StringBuilder sb  = new StringBuilder();
            for(int i=0;i<liste.size();i++) {
                    studyGroup = liste.get(i);
                    sb.append(studyGroup.getBasename()).append(";")
                            .append(studyGroup.getSemester()).append(";")
                            .append(studyGroup.getComment()).append(";")
                            .append("\n");       
            }
            fstream.write(sb.toString());
            fstream.close();
        }catch(FileNotFoundException e){
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
        }catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());    
        }
    }
        
        public void writeToSubjectFile(String path, String year, List<Subject> liste, String filename) {
        Subject subject;
        // File fileDir = buildFileDir(path, year, "subjects");
        File fileDir = buildFileDir(path, year, filename);
        
        try(Writer fstream = new OutputStreamWriter(new FileOutputStream(fileDir), StandardCharsets.UTF_8);) {
            StringBuilder sb  = new StringBuilder();
            for(int i=0;i<liste.size();i++) {
                    subject = liste.get(i);
                   
                    sb.append(subject.getShortcut()).append(";")
                            .append(subject.getStudygroupbase()).append(";")
                            .append(subject.getStudygroupsemester()).append(";")
                            .append(subject.getLongname()).append(";")
                            .append(subject.getCurriculumType()).append(";")
                            .append(subject.getDidacticType()).append(";")
                            .append(subject.getComment()).append("\n");
            }
            fstream.write(sb.toString());
            fstream.close();
        
        }catch(FileNotFoundException e){
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());
        }catch(IOException e) {
            sLogger.log(Level.WARNING, "- called. {0}", e.getMessage());    
        }
    }
        
        // Wss, 20200126, new method
    public File buildFileDir (String config_file_path, String year, String filename) {
                 // File fileDir = new File(config_file_path + year + File.separator + filename);  // original code line, should be used later after apdating stdplan
        // wss 20200126: to get stdplanconfig to work with  stdplan the value of year
        // will be temporarily ignored since  stdplan does not know years.
        File fileDir = new File(config_file_path + filename);
          return fileDir;
        }
}
