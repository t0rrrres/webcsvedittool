
package de.hscoburg.nedev.stdplanconfig;

import de.hscoburg.nedev.stdplanconfig.Models.Room;
import de.hscoburg.nedev.stdplanconfig.Models.StudyGroup;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author nesh
 */
public class AddEditServletRoom extends HttpServlet {
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Logger writeToActionLog = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
        
        String path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();
        String year = session.getAttribute("optionValueListYear").toString();
        LoadListsUtilityFunctions lluf = new LoadListsUtilityFunctions();
        String filename = ServletUtilityFunctions.getFilenameRooms(request);
        List<Room> rooms = lluf.getRoomsList(path,year, filename).getList();
        
        int index = Integer.parseInt(request.getParameter("roomID"));
        String roomName = request.getParameter("roomName");
        String roomSize = request.getParameter("roomSize");
        String roomOwner = request.getParameter("roomOwner");
        String roomType = request.getParameter("roomType");
        String roomComment = request.getParameter("roomComment");
        Boolean cloneRow = Boolean.parseBoolean(request.getParameter("cloneRow"));
        
        if(index == -1){
            Room room = new Room(roomName,roomSize, roomOwner,roomType,roomComment);
            writeToActionLog.log(Level.INFO,"User: " +request.getRemoteUser() + "- called. " + "ADD NEW ROOM : " +room.toString()); 
            rooms.add(room);
        }else{
            
            if(cloneRow){
                Room room = new Room(roomName,roomSize, roomOwner,roomType,roomComment);
                rooms.add(index+1, room);
            }else{
                Room toEdit = rooms.get(index);
                writeToActionLog.log(Level.INFO,"User: " +request.getRemoteUser() + "- called. " + "EDIT ROOM FROM: " + toEdit.toString()); 
                toEdit.setType(roomType);
                toEdit.setName(roomName);
                toEdit.setSize(roomSize);
                toEdit.setOwner(roomOwner);
                toEdit.setComment(roomComment);

                writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "EDITED ROOM TO: " + toEdit.toString()); 
                rooms.remove(index);
                rooms.add(index, toEdit);
            }   
        }
        
        lluf.writeToRoomFile(path,year, rooms, filename);
        session.setAttribute("optionValueListType", session.getAttribute("optionValueListType"));
        session.setAttribute("optionValueListYear", session.getAttribute("optionValueListYear"));
        RequestDispatcher view = request.getRequestDispatcher("/loadList");
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
}
