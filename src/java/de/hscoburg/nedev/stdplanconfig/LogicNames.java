/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;

/**
 *
 * @author dieter
 */

// wss 20191111

public class LogicNames {
    //final static public String LOCAL_PATH_CONFIG_DATA_CONTEXT_PARAM_NAME="local_path_prefix_configdata";  
    final static public String ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME="abs_path_prefix_configdata";
    final static public String STDPLAN_DEFAULT_DROPDOWN_PATH="stdplan_config_default_dropdown_path";
    final static public String LOCAL_PATH_ACTION_LOGING_CONTEXT_PARAM_NAME="stdplan_config_action_logs";
    final static public String LOCAL_PATH_CONFIG_DATA_ARCHIVE_CONTEXT_PARAM_NAME="local_path_prefix_configdata_archive";
    final static public String STDPLAN_CONFIG_ALLOWED_FILE_ENDINGS_CONTEXT_PARAM_NAME="stdplan_config_allowed_file_endings";
    //  type specific filenames
    final static public String ACTIONLOG_FILENAME_CONTEXT_PARAM_NAME="stdplan_actionlog_filename";
    final static public String LECTURERS_FILENAME_CONTEXT_PARAM_NAME="stdplan_config_lecturers_filename";
    final static public String SUBJECTS_FILENAME_CONTEXT_PARAM_NAME="stdplan_config_subjects_filename";
    final static public String STUDYGROUPS_FILENAME_CONTEXT_PARAM_NAME="stdplan_config_studygroups_filename";
    final static public String ROOMS_FILENAME_CONTEXT_PARAM_NAME="stdplan_config_rooms_filename";
    // for logging
    final static public String LOGGING_LEVEL_APP_INIT_CONTEXT_PARAM_NAME="logging_level";
    
    public static String getFileName(String filename){
        String retVal = "";
        switch(filename){
            case "lecturers":
                retVal = LECTURERS_FILENAME_CONTEXT_PARAM_NAME;
                break;
            case "rooms": 
                retVal = ROOMS_FILENAME_CONTEXT_PARAM_NAME;
                break;
            case "subjects":
                retVal = SUBJECTS_FILENAME_CONTEXT_PARAM_NAME;
                break;
            case "studygroups":
                retVal = STUDYGROUPS_FILENAME_CONTEXT_PARAM_NAME;
                break;
        }
        return retVal;
    }
}
