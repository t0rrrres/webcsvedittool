package de.hscoburg.nedev.stdplanconfig;

import de.hscoburg.nedev.stdplanconfig.Models.Room;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nesh
 */
public class DeleteServletROOM extends HttpServlet {

    public void manageRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException{
        
        Logger writeToActionLog = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
        String path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();
        String year = session.getAttribute("optionValueListYear").toString();
        LoadListsUtilityFunctions lluf = new LoadListsUtilityFunctions();
        String filename = ServletUtilityFunctions.getFilenameRooms(request);
        List<Room> rooms = lluf.getRoomsList(path, year, filename).getList();
        
        
        int num = Integer.parseInt(request.getParameter("rowID"));
        Room room = rooms.get(num);
        writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "DELETE ROOM " + year + " : " + room.toString()); 
        rooms.remove(num);
        lluf.writeToRoomFile(path, year, rooms, filename);
       
        session.setAttribute("optionValueListType", session.getAttribute("optionValueListType"));
        session.setAttribute("optionValueListYear", session.getAttribute("optionValueListYear"));
        RequestDispatcher view = request.getRequestDispatcher("/loadList");
        view.forward(request, response);
        
    }
    
      @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws IOException,ServletException{
          manageRequest(request, response);
        
    }
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws IOException,ServletException{
        manageRequest(request, response);
    }
    
       
}
