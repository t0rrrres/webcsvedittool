/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;


import java.io.File;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author dieter
 */
public class FilesystemHelper {

  private Level mLoggingLevel; // 

  private static final Logger sLogger = Logger.getLogger(FilesystemHelper.class.getPackage().getName() + ".custom");
  

  public String[] getSubdirsOfConfigPath(ServletContext app) {
    sLogger.setFilter(LoggingInferCallerFilter.getTheFilter());
    String[] res = null;
    String logTextPrefix = ": ";
    String logText = "";

    // get configured logging level  
    String loggingLevelStr = app.getInitParameter(LogicNames.LOGGING_LEVEL_APP_INIT_CONTEXT_PARAM_NAME);
    if (loggingLevelStr != null && loggingLevelStr.equals(LogicNames.LOGGING_LEVEL_APP_INIT_CONTEXT_PARAM_NAME)) {
      mLoggingLevel = Level.CONFIG; // SEVERE, WARNING, INFO, CONFIG. But not FINE etc.
    } else {
      mLoggingLevel = Level.INFO; // SEVERE, WARNING, INFO
    }
    sLogger.setLevel(mLoggingLevel);
// wss 20191111
    // Getting initialisation parameters and output them into log
    File root;
    String rootpath = app.getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);

    if (rootpath != null) {
      // check if path exists and is readable and writable
      root = new File(rootpath);
      if (root.canWrite() == false || root.isDirectory() == false) {
        rootpath = app.getRealPath(Consts.DEFAULT_CONFIG_DATA_PATH);
      }
    } else {
      rootpath = app.getRealPath(Consts.DEFAULT_CONFIG_DATA_PATH);
    }

    logText = "rootpath=" + rootpath;
    sLogger.log(Level.INFO, logTextPrefix + "- called. " + logText);
    root = new File(rootpath);
    
    // check for wrong conditions
    if (root.isDirectory() == false || root.canWrite() == false) {
      res = new String[1];
      res[0] = "given root is not a directory or is not writeable";
      return res;
    }
    if (root.listFiles().length == 0) {
      res = new String[1];
      res[0] = "given root has no subdirectories";
      return res;      
    }
    // get subdirectories
    Vector<String> dirlist = new Vector<String>();
    for (File f : root.listFiles()) {
      if (f.isDirectory()) {
        dirlist.add(f.getName());
      }
    }
    res = dirlist.toArray(new String[1]);
    return res;
  }
}
