/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nesh
 */
public class CreateSubFolder extends HttpServlet {


    private boolean copyConfigFiles(File src, File dest) {
        boolean marker = true;
        String[] filenames = {
            LogicNames.LECTURERS_FILENAME_CONTEXT_PARAM_NAME,
            LogicNames.ROOMS_FILENAME_CONTEXT_PARAM_NAME,
            LogicNames.STUDYGROUPS_FILENAME_CONTEXT_PARAM_NAME,
            LogicNames.SUBJECTS_FILENAME_CONTEXT_PARAM_NAME
        };

        for (String filename : filenames) {
            File source = new File(src + File.separator + getServletContext().getInitParameter(filename));
            File destination = new File(dest + File.separator + getServletContext().getInitParameter(filename));
            
            try {  
                Files.copy(source.toPath(),destination.toPath());
            } catch (IOException ex) {
                Logger.getLogger(CreateSubFolder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }         
        return marker;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
             {
        String config_file_path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        
        HttpSession session = request.getSession();
        File src = new File(config_file_path + getServletContext().getInitParameter(LogicNames.STDPLAN_DEFAULT_DROPDOWN_PATH));
        File dest = new File(config_file_path+request.getParameter("folderName").toUpperCase());
        if(dest.exists()){
            session.setAttribute("folderCreated", "Dieser Ordner existiert berets");
        }else{
            if(dest.mkdir()){
                dest.setWritable(true);
                copyConfigFiles(src, dest);
                session.setAttribute("folderCreated", "Der Ordner wurde erstellt");
            }else{
                session.setAttribute("folderCreated", "");
            }
        }
        RequestDispatcher view = request.getRequestDispatcher("result.jsp");
        try {
            view.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(CreateSubFolder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreateSubFolder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
