package de.hscoburg.nedev.stdplanconfig;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class LoadList extends HttpServlet {
   
    public void manageRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException{
         
        String config_file_path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();     
        session.removeAttribute("folderCreated");
        
        String optionValueListType = request.getParameter("optionValueListType");
        String optionValueListYear = request.getParameter("optionValueListYear");
        
        if(optionValueListType == null || optionValueListYear == null){
            optionValueListType= (String)session.getAttribute("optionValueListType");
            optionValueListYear= (String)session.getAttribute("optionValueListYear");    
            session.setAttribute("reloading", "true");
        }

        if("rooms".equals(optionValueListType)){
            String filename = ServletUtilityFunctions.getFilenameRooms(request);
            MixedData rooms = new LoadListsUtilityFunctions().getRoomsList(config_file_path, optionValueListYear, filename);
            session.setAttribute("rooms", rooms.getList());
            session.setAttribute("errorlist", rooms.getErrrolist());
            session.setAttribute("optionValueListType", optionValueListType);
            session.setAttribute("optionValueListYear", optionValueListYear);
            
        }else if("lecturers".equals(optionValueListType)){
          // wss 20191111
            String filename = ServletUtilityFunctions.getFilenameLecturers(request);            
            MixedData dozenten = new LoadListsUtilityFunctions().getLecturerList(config_file_path,optionValueListYear, filename);
            session.setAttribute("dozenten", dozenten.getList());
            session.setAttribute("errorlist", dozenten.getErrrolist());
            session.setAttribute("optionValueListType", optionValueListType);
            session.setAttribute("optionValueListYear", optionValueListYear);
        }
        else if("subjects".equals(optionValueListType)){
            String filename = ServletUtilityFunctions.getFilenameSubjects(request);
            MixedData subjects = new LoadListsUtilityFunctions().getSubjectsList(config_file_path, optionValueListYear, filename);
            session.setAttribute("subjects", subjects.getList());
            session.setAttribute("errorlist", subjects.getErrrolist());
            session.setAttribute("optionValueListType", optionValueListType);
            session.setAttribute("optionValueListYear", optionValueListYear);    
        }else if("studygroups".equals(optionValueListType)){
            String filename = ServletUtilityFunctions.getFilenameStudyGroups(request);
            MixedData studygroups = new LoadListsUtilityFunctions().getStudyGroupList(config_file_path, optionValueListYear, filename);
            session.setAttribute("studygroups", studygroups.getList());
            session.setAttribute("errorlist", studygroups.getErrrolist());
            session.setAttribute("optionValueListType", optionValueListType);
            session.setAttribute("optionValueListYear", optionValueListYear);
        }else{
            //If nothing maches remove all session objects
            session.removeAttribute("dozenten");
            session.removeAttribute("rooms");
            session.removeAttribute("subjects");
            session.removeAttribute("studygroups");
            session.removeAttribute("optionValueListType");
            session.removeAttribute("optionValueListYear");
            
        }
        RequestDispatcher view = request.getRequestDispatcher("result.jsp");
        view.forward(request, response);
    }
    
      @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws IOException,ServletException{
          manageRequest(request, response);
        
    }
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws IOException,ServletException{
        
        manageRequest(request, response);
    }
      
    
  
}
