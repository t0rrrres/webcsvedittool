/**
 * 
 * SERVLET FOR EDIT/ADD LECTURER
 * 
 **/
package de.hscoburg.nedev.stdplanconfig;
import de.hscoburg.nedev.stdplanconfig.Models.Lecturer;
import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;

public class AddEditServletLecturer extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
        Logger writeToActionLog = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
        
        String config_file_path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();
        String year = session.getAttribute("optionValueListYear").toString();
        LoadListsUtilityFunctions lluf = new LoadListsUtilityFunctions();
        String filename = ServletUtilityFunctions.getFilenameLecturers(request);  
        List<Lecturer> dozenten =  lluf.getLecturerList(config_file_path,year,filename).getList();
        
        int index = Integer.parseInt(request.getParameter("userID"));
        String lecturerFirstName = request.getParameter("lecturerFirstName");
        String lecturerLastName = request.getParameter("lecturerLastName");
        String lecturerAbbr = request.getParameter("lecturerAbbr");
        String lecturerType = request.getParameter("lecturerType");
        String lecturerComment = request.getParameter("lecturerComment");
        Boolean cloneRow = Boolean.parseBoolean(request.getParameter("cloneRow"));
        
        String activeUser = request.getRemoteUser();
        
        if(index == -1){
            Lecturer lec = new Lecturer(lecturerAbbr,lecturerLastName, lecturerFirstName,lecturerType,lecturerComment);
            writeToActionLog.log(Level.INFO, "User: " + activeUser +  "- called. " + "ADD NEW LECTURER: " +lec.toString());
           
            dozenten.add(lec);
        }else{
            if(cloneRow){
                Lecturer lec = new Lecturer(lecturerAbbr,lecturerLastName, lecturerFirstName,lecturerType,lecturerComment);
                dozenten.add(index+1, lec);
            }else{
                Lecturer toEdit = dozenten.get(index);
                writeToActionLog.log(Level.INFO, "User: " +activeUser + "- called. " + "EDIT LECTURER FROM: " + toEdit.toString()); 
                toEdit.SetType(lecturerType);
                toEdit.setAbbr(lecturerAbbr);
                toEdit.setFirstName(lecturerFirstName);
                toEdit.setLastName(lecturerLastName);
                toEdit.setComment(lecturerComment);
                writeToActionLog.log(Level.INFO, "User: " +activeUser + "- called. " + "EDITED LECTURER TO: " + lecturerAbbr + "\t"+ lecturerLastName + "\t" + lecturerFirstName + "\t" + lecturerType); 
            
                dozenten.remove(index);
                dozenten.add(index, toEdit);
            }       
        }
       
        session.setAttribute("optionValueListType", session.getAttribute("optionValueListType"));
        session.setAttribute("optionValueListYear", session.getAttribute("optionValueListYear"));   
        lluf.writeToLecturerFile(config_file_path, year, dozenten, filename);
        
        RequestDispatcher view = request.getRequestDispatcher("/loadList");
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
