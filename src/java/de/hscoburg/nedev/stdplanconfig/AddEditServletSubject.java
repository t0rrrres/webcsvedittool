package de.hscoburg.nedev.stdplanconfig;

import de.hscoburg.nedev.stdplanconfig.Models.StudyGroup;
import de.hscoburg.nedev.stdplanconfig.Models.Subject;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddEditServletSubject extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Logger writeToActionLog = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
        
        String path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();
        String year = session.getAttribute("optionValueListYear").toString();
        LoadListsUtilityFunctions lluf = new LoadListsUtilityFunctions();
        String filename = ServletUtilityFunctions.getFilenameSubjects(request);
        List<Subject> subjects = lluf.getSubjectsList(path, year,filename).getList();
        
        int index = Integer.parseInt(request.getParameter("subjectID"));
        String sbjectStudygroup = request.getParameter("sbjectStudygroup");
        String subjectFullname = request.getParameter("subjectFullname");
        String subjectAbbr = request.getParameter("subjectAbbr");
        String subjectCurType = request.getParameter("subjectCurType");
        String subjectSemester = request.getParameter("subjectSemester");
        String subjectDiType = request.getParameter("subjectDiType");
        String subjectComment = request.getParameter("subjectComment");
        Boolean cloneRow = Boolean.parseBoolean(request.getParameter("cloneRow"));
        
        if(index == -1){
            Subject subject = new Subject(subjectAbbr,sbjectStudygroup, subjectSemester,subjectFullname,subjectCurType,subjectDiType,subjectComment );
            writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "ADD NEW SUBJECT: " +subject.toString());
            subjects.add(subject);
        }else{
            if(cloneRow){
                Subject subject = new Subject(subjectAbbr,sbjectStudygroup, subjectSemester,subjectFullname,subjectCurType,subjectDiType,subjectComment );
                subjects.add(index+1, subject);
            }else{
                Subject toEdit = subjects.get(index);
                writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "EDIT SUBJECT FROM: " + toEdit.toString());
                toEdit.setShortcut(subjectAbbr);
                toEdit.setStudygroupbase(sbjectStudygroup);
                toEdit.setStudygroupsemester(subjectSemester);
                toEdit.setLongname(subjectFullname);
                toEdit.setCurriculumType(subjectCurType);
                toEdit.setDidacticType(subjectDiType);
                toEdit.setComment(subjectComment);

                writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "EDITED ROOM TO: " + toEdit.toString());
                subjects.remove(index);
                subjects.add(index, toEdit);
            }
        }
        
        lluf.writeToSubjectFile(path, year, subjects, filename); 
       
        session.setAttribute("optionValueListType", session.getAttribute("optionValueListType"));
        session.setAttribute("optionValueListYear", session.getAttribute("optionValueListYear"));
        RequestDispatcher view = request.getRequestDispatcher("/loadList");
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
