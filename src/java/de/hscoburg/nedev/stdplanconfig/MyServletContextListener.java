package de.hscoburg.nedev.stdplanconfig;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.servlet.ServletContext;


public class MyServletContextListener implements ServletContextListener {
    private FileHandler fileHandler;
    private Logger logger;
    
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //ServletContextListener.super.contextDestroyed(sce); //To change body of generated methods, choose Tools | Templates.
        logger.removeHandler(fileHandler);
        fileHandler.close();
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
           // ServletContextListener.super.contextInitialized(sce); //To change body of generated methods, choose Tools | Templates.

            ServletContext app = sce.getServletContext();
            String log_file_path = app.getInitParameter(LogicNames.LOCAL_PATH_ACTION_LOGING_CONTEXT_PARAM_NAME);
            String log_file_name = app.getInitParameter(LogicNames.ACTIONLOG_FILENAME_CONTEXT_PARAM_NAME);
            String realPath = app.getRealPath(log_file_path)+log_file_name;
            fileHandler = new FileHandler(realPath + ".log", true);
            
            logger = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
            logger.setUseParentHandlers(false);
            logger.setFilter(LoggingInferCallerFilter.getTheFilter());
                      
            fileHandler.setFormatter(new SimpleFormatter()); 
            logger.addHandler(fileHandler);
            
        } catch (IOException ex) {
            Logger.getLogger(MyServletContextListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(MyServletContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
