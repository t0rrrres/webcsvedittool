/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

/**
 *
 * @author nesh
 */
public class LoggingInferCallerFilter implements Filter {

    static private LoggingInferCallerFilter theOnlyOne = new LoggingInferCallerFilter();


    static public LoggingInferCallerFilter getTheFilter() {
        return theOnlyOne;
    }
    
    @Override
    public boolean isLoggable(LogRecord record) {
        record.getSourceMethodName();
        return true;
    }
    
}
