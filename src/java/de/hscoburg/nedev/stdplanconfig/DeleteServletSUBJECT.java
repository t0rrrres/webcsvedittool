package de.hscoburg.nedev.stdplanconfig;

import de.hscoburg.nedev.stdplanconfig.Models.Subject;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class DeleteServletSUBJECT extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Logger writeToActionLog = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
        
        String path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();
        String year = session.getAttribute("optionValueListYear").toString();
        LoadListsUtilityFunctions lluf = new LoadListsUtilityFunctions();
        String filename = ServletUtilityFunctions.getFilenameSubjects(request);
        List<Subject> subjects = lluf.getSubjectsList(path, year, filename).getList();
            
        int num = Integer.parseInt(request.getParameter("rowID"));
        Subject subject = subjects.get(num);
        writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "DELETE SUBJECT " +year + ": " + subject.toString()); 
        subjects.remove(num);
        lluf.writeToSubjectFile(path,year, subjects, filename);
              
        session.setAttribute("optionValueListType", session.getAttribute("optionValueListType"));
        session.setAttribute("optionValueListYear", session.getAttribute("optionValueListYear"));
        RequestDispatcher view = request.getRequestDispatcher("/loadList");
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
