/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hscoburg.nedev.stdplanconfig;

/**
 *
 * @author dieter
 */

// wss 20191111
public class Consts {

  final static public String APP_ROOT_ALIAS = "/";
  final static public String PROJECT_VERSION_SEPARATOR = "-";

  final static public String DEFAULT_CONFIG_DATA_PATH = "/WEB-INF/data/config";
  final static public String DEFAULT_CONFIG_DATA_BACKUP_PATH = "/WEB-INF/data/backup";

  
  // Default filenames
  final static public String DEFAULT_LECTURERS_FILENAME = "lecturers";
  final static public String DEFAULT_ROOMS_FILENAME = "rooms";
  final static public String DEFAULT_SUBJECTS_FILENAME = "subjects";
  final static public String DEFAULT_STUDYGROUPS_FILENAME = "studygroups";
  

}
