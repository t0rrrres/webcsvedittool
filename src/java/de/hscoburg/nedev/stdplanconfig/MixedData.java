/**
 * 
 * Helpclass returning configurationlist data 
 * and error rows data
 * 
 */
package de.hscoburg.nedev.stdplanconfig;

import java.util.List;

/**
 *
 * @author nesh
 */
public class MixedData {
    
    private final List datalist;
    private final String errorRows;
    
    public MixedData(List al, String str){
        datalist = al;
        errorRows = str;
    }
    
    public List getList(){
        return datalist;
    }
    
    public String getErrrolist(){
        return errorRows;
    }
    
    
    
    
}
