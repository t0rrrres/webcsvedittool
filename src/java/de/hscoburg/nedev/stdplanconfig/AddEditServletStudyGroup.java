package de.hscoburg.nedev.stdplanconfig;

import de.hscoburg.nedev.stdplanconfig.Models.StudyGroup;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AddEditServletStudyGroup extends HttpServlet {

 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Logger writeToActionLog = Logger.getLogger(this.getClass().getPackage().getName() + ".custom");
 
        String path = getServletContext().getInitParameter(LogicNames.ABS_PATH_CONFG_DATA_CONTEXT_PARAM_NAME);
        HttpSession session = request.getSession();
        String year = session.getAttribute("optionValueListYear").toString();
        LoadListsUtilityFunctions lluf = new LoadListsUtilityFunctions();
        String filename = ServletUtilityFunctions.getFilenameStudyGroups(request);
        List<StudyGroup> studyGroups = lluf.getStudyGroupList(path, year, filename).getList();
        
        int index = Integer.parseInt(request.getParameter("studyGroupID"));
        String baseName = request.getParameter("baseName");
        String semester = request.getParameter("semester");
        String comment = request.getParameter("comment");
        Boolean cloneRow = Boolean.parseBoolean(request.getParameter("cloneRow"));
        
        if(index == -1){
            StudyGroup sg = new StudyGroup(baseName,semester, comment);
            writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "ADD NEW STUDY GROUP: " +sg.toString());
            studyGroups.add(sg);
        }else{
            if(cloneRow){
                StudyGroup sg = new StudyGroup(baseName,semester, comment);
                studyGroups.add(index+1, sg);
            }else{
                StudyGroup toEdit = studyGroups.get(index);
                writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "EDIT ROOM FROM: " + toEdit.toString());
                toEdit.setBasename(baseName);
                toEdit.setSemester(semester);
                toEdit.setComment(comment);

                writeToActionLog.log(Level.INFO, "User: " +request.getRemoteUser() + "- called. " + "EDITED ROOM TO: " + toEdit.toString());
                studyGroups.remove(index);
                studyGroups.add(index, toEdit);
            }        
        }
        
        lluf.writeToStudyGroupFile(path,year, studyGroups, filename);
        
        session.setAttribute("optionValueListType", session.getAttribute("optionValueListType"));
        session.setAttribute("optionValueListYear", session.getAttribute("optionValueListYear"));
        
        RequestDispatcher view = request.getRequestDispatcher("/loadList");
        view.forward(request, response);
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
