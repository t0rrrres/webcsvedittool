$(document).ready(function() {
     
    //  INIT DATATABLES -->
    let dt  = $('#tabelle').DataTable({
        responsive: true,
        "initComplete": afterTabInit,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "columnDefs": [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: -1 }
        ],  
        "fixedColumns":   {
            leftColumns: 0,
            rightColumns: 1
        },
        "stateSave": true,
        "lengthMenu": [ 10, 25, 50 ],
        "pageLength": 25
    } );
    
    
    // Delete EventListener on ActionButtons
    $("[id*=Modal]").on("hide.bs.modal",deleteEvent);
    function deleteEvent(){
        $("[id*=Modal] #actionBtn").off("click");
    }
    
    $(document).on("click","#burgerBtn",switchMenuState);
    function switchMenuState() {
        var x = document.getElementById("mylinks");
        if (x.style.display === "block") {
            sessionStorage.setItem("burgerMenuState","none");
            x.style.display = "none";
        } else {
            sessionStorage.setItem("burgerMenuState","block");
            x.style.display = "block";
        }
    }
            
    function afterTabInit(){
        console.log( "De Tabelle ist geladen" );
    }
    // INIT HIDE MODALS
    let options ={
        show:false
    };
    $('#basicModalAddEditLecturer').modal(options);
    $('#basicModalDeleteLecturer').modal(options);
    $('#basicModalAddEditRoom').modal(options);
    $('#basicModalDeleteRoom').modal(options);
    
       //LOGOUT
    $(document).on("click","#logout",function(){        
        window.location = "Logout";
    });
    
    function clearInputFiledBorder(modal){
        $(modal + ' .required').each(function(){
            $(this).removeAttr('style'); 
        });
    }
    
    function checkInputFieldsIfEmpty(modal){ 
        $(modal + ' .required').each(function(){
            if($(this).val()===""){
               $(this).css('border','1px solid');
               $(this).css('border-color','red');
            }else{
               $(this).css('border-color','green');
            }    
        });  
    };
    
/** ###################### START CREATE NEW FOLDER ######################### **/

     // CREATE NEW FOLDER
    $(document).on("click","#createFolder",function(){
        $("#basicModalCreateSubFolder #myModalLabel").text('');
        $("#basicModalCreateSubFolder #myModalLabel").append('Neuer Ordner erstellen');
        $("#basicModalCreateSubFolder .modal-body #folderName").val("");
        clearInputFiledBorder('#basicModalCreateSubFolder');
        $('#basicModalCreateSubFolder').modal('show');
       
        $('#basicModalCreateSubFolder #actionBtn').click(function(){
            let folderName = $("#basicModalCreateSubFolder .modal-body #folderName").val();
            createFolder(folderName);
        });
    });
    
    function createFolder(folderName) {
        let optionValueListType = $("#selectListType").val();
        let optionValueListYear = $("#selectListYear").val(); 
        $.ajax({
            type: "get",
            url: "CreateSubFolder", //here you can use servlet,jsp, etc
            data: {
                "optionValueListType":optionValueListType,
                "optionValueListYear":optionValueListYear,
                "folderName":folderName
            },
            success: function(){  
                // Before reloading the page, set "RELOADING" to true as marker 
                // Store which options have been clicked 
                sessionStorage.setItem("reloading","true");
                sessionStorage.setItem("optionValueListType",optionValueListType);
                sessionStorage.setItem("optionValueListYear",optionValueListYear);
                location.reload(true);         
            }
        });
   }

     /** ###################### END CREATE NEW FOLDER ######################### **/

    
    /** ################## START LOADLIST BUTTON ###################### **/
    // set eventhandler for click event
    $(document).on("click","#loadListBtn",load_data_set);
    // set eventhandler for change event
    $(document).on("change","#selectListType",load_data_set);
    $(document).on("change","#selectListYear",load_data_set);
    
    // Check If RELOAD Variable is set  
    //If true call function set_select_selected() to set "selected" attribute on drop-down select tag  
    function setSelected(){
        let reloading = sessionStorage.getItem("reloading");
        if(reloading){   
            set_select_selected();
            sessionStorage.removeItem("reloading");
             sessionStorage.removeItem("optionValueListType");
              sessionStorage.removeItem("optionValueListYear");
        }
    }
    setSelected();

   // Helper functions
   function load_data_set() {
        let optionValueListType = $( "#selectListType").val();
        let optionValueListYear = $("#selectListYear").val(); 
        $.ajax({
            type: "get",
            url: "loadList", //here you can use servlet,jsp, etc
            data: {
                "optionValueListType":optionValueListType,
                "optionValueListYear":optionValueListYear
            },
            success: function(){  
                // Before reloading the page, set "RELOADING" to true as marker 
                // Store which options have been clicked 
                sessionStorage.setItem("reloading","true");
                sessionStorage.setItem("optionValueListType",optionValueListType);
                sessionStorage.setItem("optionValueListYear",optionValueListYear);
                location.reload(true);           
            }
        });
   }
   
   // go through all select-options and check which one has been clicked
    function set_select_selected(){
        let optionValueListType = sessionStorage.getItem("optionValueListType");
        let optionValueListYear = sessionStorage.getItem("optionValueListYear");
        $('select > option').each(function() {
            if($(this).val()=== optionValueListType){ 
                $(this).attr('selected','selected');
            }
             if($(this).val()=== optionValueListYear){ 
                $(this).attr('selected','selected'); 
            }
        }); 
    }    
      
/** #################### END LOADLIST BUTTON - NAVBAR ####################### **/

    // DELETE Row Function for all Servlets
    function deleteRow(rowID,serv){
        let optionValueListType = $( "#selectListType").val();
        let optionValueListYear = $("#selectListYear").val();
        $.ajax({
            type: "get",
            url: serv, //here you can use servlet,jsp, etc  
            data: {
                "rowID":+rowID     
            },
            success: function(){
                sessionStorage.setItem("reloading","true");
                sessionStorage.setItem("optionValueListType",optionValueListType);
                sessionStorage.setItem("optionValueListYear",optionValueListYear);
                location.reload(true);                 
            }
        });
    }
    
/** #####################  START LECTURER Action-Column    ##################### **/

    $(document).on("click","#addButtonLecturerModal",{type:"add"}, lecturerActionButtons);
    $(document).on("click","#editButtonLecturerModal",{type:"edit"}, lecturerActionButtons);
    $(document).on("click","#copyDataRowButtonLecturerModal",{type:"clone"}, lecturerActionButtons);
    $(document).on("click", "#deleteButtonLecturerModal", lecturerDeleteActionButton);
    
    //Call AddEditServlet Lecturer
    function editAddRowLecturer(rowID,firstname,lastname,abbr,type,comment,cloneRow){
        let lecturerId = +rowID;
        let lecturerFirstName = firstname===""?" ":firstname;
        let lecturerLastName = lastname;
        let lecturerAbbr = abbr;
        let lecturerType = type===""?" ":type;
        let lecturerComment = comment===""?" ":comment;   
        
        //Nach Pflichtfelder prüfen
        if(lecturerLastName!=="" && lecturerAbbr!==""){
            let optionValueListType = $( "#selectListType").val();
            let optionValueListYear = $("#selectListYear").val();
            $.ajax({
                type: "get",
                url: "AddEditServlet",
                data: {
                    "userID":lecturerId,
                    "lecturerFirstName":lecturerFirstName,
                    "lecturerLastName":lecturerLastName,
                    "lecturerAbbr":lecturerAbbr,
                    "lecturerType":lecturerType,
                    "lecturerComment":lecturerComment,
                    "cloneRow":cloneRow
                },
                success: function(){  
                    sessionStorage.setItem("reloading","true");
                    sessionStorage.setItem("optionValueListType",optionValueListType);
                    sessionStorage.setItem("optionValueListYear",optionValueListYear);
                    location.reload(true);           
                }
            });
        }else{
            checkInputFieldsIfEmpty('#basicModalAddEditLecturer');          
        }      
    }
    
    // EDIT Button from LECTURER Action-Column
    function lecturerActionButtons(event){
        
        let eventType = event.data.type;      
        let lecturerId = $(this).data('index');
        let lecturerFirstName = $(this).data('firstname');
        let lecturerLastName = $(this).data('lastname');
        let lecturerAbbr = $(this).data('abbr');
        let lecturerType = $(this).data('type');
        let lecturerComment = $(this).data('comment');
        
        $("#basicModalAddEditLecturer #myModalLabel").text('');
        clearInputFiledBorder('#basicModalAddEditLecturer');  
        
        if(eventType==="add"){  
            $("#basicModalAddEditLecturer #myModalLabel").append('Neuer Eintrag Dozent');
            $("#basicModalAddEditLecturer .modal-body #first_name").val("");
            $("#basicModalAddEditLecturer .modal-body #last_name").val("");
            $("#basicModalAddEditLecturer .modal-body #abbr").val("");
            $("#basicModalAddEditLecturer .modal-body #type").val("");
            $("#basicModalAddEditLecturer .modal-body #comment").val("");
            
        }else{
            if(eventType==="edit"){
                $("#basicModalAddEditLecturer #myModalLabel").append('Änderung');
            }
            if(eventType==="clone"){
                $("#basicModalAddEditLecturer #myModalLabel").append('Duplizierung');
            }
            $("#basicModalAddEditLecturer .modal-body #first_name").val(lecturerFirstName);
            $("#basicModalAddEditLecturer .modal-body #last_name").val(lecturerLastName);
            $("#basicModalAddEditLecturer .modal-body #abbr").val(lecturerAbbr);
            $("#basicModalAddEditLecturer .modal-body #type").val(lecturerType);
            $("#basicModalAddEditLecturer .modal-body #comment").val(lecturerComment);
        }        
        
        $('#basicModalAddEditLecturer').modal('show');
        
        $('#basicModalAddEditLecturer #actionBtn').click(function(){
            let lecturerFirstName = $("#basicModalAddEditLecturer .modal-body #first_name").val();
            let lecturerLastName = $("#basicModalAddEditLecturer .modal-body #last_name").val();
            let lecturerAbbr = $("#basicModalAddEditLecturer .modal-body #abbr").val();
            let lecturerType = $("#basicModalAddEditLecturer .modal-body #type").val();
            let lecturerComment = $("#basicModalAddEditLecturer .modal-body #comment").val(); 
            
            if(eventType==="add"){
                editAddRowLecturer("-1",lecturerFirstName,lecturerLastName,lecturerAbbr,lecturerType,lecturerComment,false);
            }
            if(eventType==="edit"){
                editAddRowLecturer(lecturerId,lecturerFirstName,lecturerLastName,lecturerAbbr,lecturerType,lecturerComment,false);
            }
            if(eventType==="clone"){      
                editAddRowLecturer(lecturerId,lecturerFirstName,lecturerLastName,lecturerAbbr,lecturerType,lecturerComment,true);                
            }      
        });        
    }
    
    // DELETE Button from LECTURER Action-Column
    function lecturerDeleteActionButton(){
        let lecturerId = $(this).data('index');
        let lecturerFirstName = $(this).data('firstname');
        let lecturerLastName = $(this).data('lastname');
        let lecturerAbbr = $(this).data('abbr');

        $("#basicModalDeleteLecturer #myModalLabel").text('');
        $("#basicModalDeleteLecturer #myModalLabel").append('LÖSCHEN');
      
        $('#deleteInfo').text("Möchten Sie wirklich " + lecturerFirstName + "-" +lecturerLastName + "["+lecturerAbbr + "] " + "löschen?" );
        $('#basicModalDeleteLecturer').modal('show');

        $('#basicModalDeleteLecturer #actionBtn').click(function(){
            deleteRow(lecturerId,"DeleteServletLECTURER");
        });
    }
/** ####################  END  LECTURER Action-Column    ##################### **/

    
    
/** #####################  START  ROOM Action-Column    ##################### **/

    $(document).on("click","#addButtonRoomModal",{type:"add"}, roomActionButtonsFn);
    $(document).on("click","#editButtonRoomModal",{type:"edit"}, roomActionButtonsFn);
    $(document).on("click","#copyDataRowButtonRoomModal",{type:"clone"}, roomActionButtonsFn);
    $(document).on("click", "#deleteButtonRoomModal", roomDeleteActionButton);

    // SAVE CHANGES Button from the EditRoom Modal Window
    function editAddRowRoom(_roomId,_roomName,_roomSize,_roomOwner,_roomType,_roomComment,_cloneRow){
        let roomID = +_roomId;
        let roomName = _roomName;
        let roomSize = _roomSize===""?" ":_roomSize;
        let roomOwner = _roomOwner===""?" ":_roomOwner;
        let roomType = _roomType;
        let roomComment = _roomComment==="" ?" ":_roomComment;
        if(roomName!=="" && roomType!==""){
            let optionValueListType = $( "#selectListType").val();
            let optionValueListYear = $("#selectListYear").val();
            $.ajax({
                type: "get",
                url: "AddEditServletRoom", //here you can use servlet,jsp, etc
                data: {
                    "roomID":roomID,
                    "roomName":roomName,
                    "roomSize":roomSize,
                    "roomOwner":roomOwner,
                    "roomType":roomType,
                    "roomComment":roomComment,
                    "cloneRow":_cloneRow
                },
                success: function(){ 
                        sessionStorage.setItem("reloading","true");
                        sessionStorage.setItem("optionValueListType",optionValueListType);
                        sessionStorage.setItem("optionValueListYear",optionValueListYear);
                        location.reload(true);  
                }
            });
        }else{
            checkInputFieldsIfEmpty('#basicModalAddEditRoom'); 
        }   
    }
    
    //  EDIT Button from ROOM Action-Column
    function roomActionButtonsFn(event){
        
        let eventType = event.data.type;
        let roomId = $(this).data('index');
        let roomName = $(this).data('name');
        let roomSize = $(this).data('size');
        let roomOwner = $(this).data('owner');
        let roomType = $(this).data('type');
        let roomComment = $(this).data('comment');
        $("#basicModalAddEditRoom #myModalLabel").text('');
        clearInputFiledBorder('#basicModalAddEditRoom');
        
        if(eventType==="add"){
            $("#basicModalAddEditRoom #myModalLabel").append('Neuer Eintrag Raum');
            $("#basicModalAddEditRoom .modal-body #name").val("");
            $("#basicModalAddEditRoom .modal-body #size").val("");
            $("#basicModalAddEditRoom .modal-body #owner").val("");
            $("#basicModalAddEditRoom .modal-body #type").val("");
            $("#basicModalAddEditRoom .modal-body #comment").val("");
        }else{
            if(eventType==="edit"){
            $("#basicModalAddEditRoom #myModalLabel").append('Änderung');
            }
            if(eventType==="clone"){
                $("#basicModalAddEditRoom #myModalLabel").append('Duplizierung');
            }

            $("#basicModalAddEditRoom .modal-body #name").val(roomName);
            $("#basicModalAddEditRoom .modal-body #size").val(roomSize);
            $("#basicModalAddEditRoom .modal-body #owner").val(roomOwner);
            $("#basicModalAddEditRoom .modal-body #type").val(roomType);
            $("#basicModalAddEditRoom .modal-body #comment").val(roomComment);
        }

        $('#basicModalAddEditRoom').modal('show');
        
        $('#basicModalAddEditRoom #actionBtn').click(function(){
            roomName = $("#basicModalAddEditRoom .modal-body #name").val();
            roomSize = $("#basicModalAddEditRoom .modal-body #size").val();
            roomOwner = $("#basicModalAddEditRoom .modal-body #owner").val();
            roomType = $("#basicModalAddEditRoom .modal-body #type").val();
            roomComment = $("#basicModalAddEditRoom .modal-body #comment").val();    
              
            if(eventType==="add"){
                editAddRowRoom("-1",roomName,roomSize,roomOwner,roomType,roomComment,false);
            }
            if(eventType==="edit"){
                editAddRowRoom(roomId,roomName,roomSize,roomOwner,roomType,roomComment,false);
            }
            if(eventType==="clone"){      
                editAddRowRoom(roomId,roomName,roomSize,roomOwner,roomType,roomComment,true);               
            }
        });        
    }
    
    // DELETE Button from ROOM Action-Column
    function roomDeleteActionButton(){
        let roomId = $(this).data('index');
        let roomName = $(this).data('name');
        let roomSize = $(this).data('size');
        let roomOwner = $(this).data('owner');
        $("#basicModalDeleteRoom #myModalLabel").text('');
        $("#basicModalDeleteRoom #myModalLabel").append('LÖSCHEN');
      
        $('#basicModalDeleteRoom #deleteInfo').text("Möchten Sie wirklich " + roomName + " - " +roomSize + " [" +roomOwner + "] " + "löschen?" );
        $('#basicModalDeleteRoom').modal('show');

        $('#basicModalDeleteRoom #actionBtn').click(function(){
            deleteRow(roomId,"DeleteServletROOM");
        });
    }
    /** ####################  END  ROOM Action-Column    ##################### **/
    
    
    
    /** ################### START  STUDYGROUP Action-Column   ################ **/
    
    $(document).on("click","#addButtonStudyGroupModal",{type:"add"}, StudyGroupActionButtons);
    $(document).on("click","#editButtonStudyGroupModal",{type:"edit"}, StudyGroupActionButtons);
    $(document).on("click","#copyDataRowButtonStudyGroupModal",{type:"clone"}, StudyGroupActionButtons);
    $(document).on("click", "#deleteButtonStudyGroupModal", studyGroupDeleteActionButton);
    
     // SAVE Button from the EditStudyGroup Modal Window
    function editRowStudyGroup(_studyGroupId, _baseName, _semester, _comment,_cloneRow){
        let studyGroupID = +_studyGroupId;
        let baseName = _baseName;
        let semester = _semester;
        let comment = _comment;
       
        if(baseName!=="" && semester!==""){
            let optionValueListType = $( "#selectListType").val();
            let optionValueListYear = $("#selectListYear").val();
            $.ajax({
                type: "get",
                url: "AddEditServletStudyGroup", //here you can use servlet,jsp, etc
                data: {
                    "studyGroupID":studyGroupID,
                    "baseName":baseName,
                    "semester":semester,
                    "comment":comment,
                    "cloneRow":_cloneRow
                },
                success: function(){  
                        sessionStorage.setItem("reloading","true");
                        sessionStorage.setItem("optionValueListType",optionValueListType);
                        sessionStorage.setItem("optionValueListYear",optionValueListYear);
                        location.reload(true); 
                }
            });
        }else{
            checkInputFieldsIfEmpty('#basicModalAddEditStudyGroup');
        } 
    }  

    function StudyGroupActionButtons(event){
        
        let eventType = event.data.type;
        let studyGroupId = $(this).data('index');
        let studyGroupBasename = $(this).data('basename');
        let studyGroupSemester = $(this).data('semester');
        let studyGroupComment = $(this).data('comment');
        $("#basicModalAddEditStudyGroup #myModalLabel").text('');
        clearInputFiledBorder('#basicModalAddEditStudyGroup');
        
        if(eventType==="add"){
            $("#basicModalAddEditStudyGroup #myModalLabel").append('Neuer Eintrag Studiengruppe');
            $("#basicModalAddEditStudyGroup .modal-body #basename").val("");
            $("#basicModalAddEditStudyGroup .modal-body #stdGroupSemester").val("");
            $("#basicModalAddEditStudyGroup .modal-body #stdGroupComment").val("");
        }else{
            if(eventType==="edit"){
            $("#basicModalAddEditStudyGroup #myModalLabel").append('Änderung');
            }
            if(eventType==="clone"){
                $("#basicModalAddEditStudyGroup #myModalLabel").append('Duplizierung');
            }

            $("#basicModalAddEditStudyGroup .modal-body #basename").val(studyGroupBasename);
            $("#basicModalAddEditStudyGroup .modal-body #stdGroupSemester").val(studyGroupSemester); 
            $("#basicModalAddEditStudyGroup .modal-body #stdGroupComment").val(studyGroupComment);
        }

        $('#basicModalAddEditStudyGroup').modal('show');
        
        $('#basicModalAddEditStudyGroup #actionBtn').click(function(){
            let studyGroupBasename = $("#basicModalAddEditStudyGroup .modal-body #basename").val();
            let studyGroupSemester = $("#basicModalAddEditStudyGroup .modal-body #stdGroupSemester").val();
            let studyGroupComment = $("#basicModalAddEditStudyGroup .modal-body #stdGroupComment").val();  
                
            if(eventType==="add"){
                editRowStudyGroup("-1",studyGroupBasename,studyGroupSemester, studyGroupComment, false);
            }
            if(eventType==="edit"){
                editRowStudyGroup(studyGroupId,studyGroupBasename,studyGroupSemester, studyGroupComment ,false);
            }
            if(eventType==="clone"){      
                editRowStudyGroup(studyGroupId,studyGroupBasename,studyGroupSemester, studyGroupComment ,true);                
            }
        });        
    }
    
    // DELETE Button from StudyGroup Action-Column
    function studyGroupDeleteActionButton(){
        let studyGroupId = $(this).data('index');
        let studyGroupBasename = $(this).data('basename');
        let studyGroupSemester = $(this).data('semester');
        $("#basicModalDeleteStudyGroup #myModalLabel").text('');
        $("#basicModalDeleteStudyGroup #myModalLabel").append('LÖSCHEN');
        $('#basicModalDeleteStudyGroup #deleteInfo').text("Möchten Sie wirklich " + "name: " +studyGroupBasename + " - " +"Semester: " +studyGroupSemester + " löschen?" );
        $('#basicModalDeleteStudyGroup').modal('show');

        $('#basicModalDeleteStudyGroup #actionBtn').click(function(){
            deleteRow(studyGroupId,"DeleteServletSTUDYGROUP");
        });
    }
    /** ################  END  STUDYGROUP Action-Column  ################### **/
    
    
    
    /** ################### START  SUBJECT Action-Column   ################ **/
    
    $(document).on("click", "#addButtonSubjectModal",{type:"add"}, subjectEditCloneActionButtons);
    $(document).on("click", "#editButtonSubjectModal",{type:"edit"}, subjectEditCloneActionButtons);
    $(document).on("click", "#copyDataRowButtonSubjectModal",{type:"clone"}, subjectEditCloneActionButtons);
    $(document).on("click", "#deleteButtonSubjectModal", subjectDeleteActionButton);
    
     // SAVE Button from the SUBJECT Modal Window
    function editRowSubject(_subjectId,_subjectAbbr,_subjectFullname,_subjectStudygroup,_subjectCurType,_subjectSemester,_subjectDiType,_subjectComment,_cloneRow){
        let subjectId = +_subjectId;

        if(_subjectAbbr!=="" &&  _subjectFullname!=="" && _subjectStudygroup!=="" && _subjectSemester!==""){
            let optionValueListType = $( "#selectListType").val();
            let optionValueListYear = $("#selectListYear").val();
            $.ajax({
                type: "get",
                url: "AddEditServletSubject", //here you can use servlet,jsp, etc
                data: {
                    "subjectID":subjectId,
                    "sbjectStudygroup":_subjectStudygroup,
                    "subjectFullname":_subjectFullname,
                    "subjectAbbr":_subjectAbbr,
                    "subjectCurType": (_subjectCurType===""?" ":_subjectCurType),
                    "subjectSemester":_subjectSemester,       
                    "subjectDiType":(_subjectDiType===""?" ":_subjectDiType),
                    "subjectComment":(_subjectComment===""?" ":_subjectComment),
                    "cloneRow":_cloneRow
                },
                success: function(){ 
                    sessionStorage.setItem("reloading","true");
                    sessionStorage.setItem("optionValueListType",optionValueListType);
                    sessionStorage.setItem("optionValueListYear",optionValueListYear);
                    location.reload(true);   
                }
            });
        }else{
            checkInputFieldsIfEmpty('#basicModalAddEditSubject');
        } 
    }    
     
    function subjectEditCloneActionButtons(event){
        
        let eventType = event.data.type;
        let subjectId = $(this).data('index');
        let subjectAbbr = $(this).data('abbr');
        let subjectFullname = $(this).data('fullname');
        let subjectStudygroup = $(this).data('studygroup');
        let subjectSemester = $(this).data('semester');
        let subjectCurType = $(this).data('curtype');
        let subjectDiType = $(this).data('ditype');
        let subjectComment = $(this).data('comment');
        $("#basicModalAddEditSubject #myModalLabel").text('');
        clearInputFiledBorder('#basicModalAddEditStudyGroup');
        
        if(eventType==="add"){
            $("#basicModalAddEditSubject #myModalLabel").append('Neuer Eintrag Modul');
            $("#basicModalAddEditSubject .modal-body #abbr").val("");
            $("#basicModalAddEditSubject .modal-body #fullname").val("");
            $("#basicModalAddEditSubject .modal-body #studygroup").val("");
            $("#basicModalAddEditSubject .modal-body #semester").val("");
            $("#basicModalAddEditSubject .modal-body #curtype").val("");
            $("#basicModalAddEditSubject .modal-body #ditype").val("");
            $("#basicModalAddEditSubject .modal-body #comment").val("");
        }else{
            if(eventType==="edit"){
                $("#basicModalAddEditSubject #myModalLabel").append('Änderung');
            }
            if(eventType==="clone"){
                $("#basicModalAddEditSubject #myModalLabel").append('Duplizierung');
            }

            $("#basicModalAddEditSubject .modal-body #abbr").val(subjectAbbr);
            $("#basicModalAddEditSubject .modal-body #fullname").val(subjectFullname);
            $("#basicModalAddEditSubject .modal-body #studygroup").val(subjectStudygroup); 
            $("#basicModalAddEditSubject .modal-body #semester").val(subjectSemester); 
            $("#basicModalAddEditSubject .modal-body #curtype").val(subjectCurType); 
            $("#basicModalAddEditSubject .modal-body #ditype").val(subjectDiType); 
            $("#basicModalAddEditSubject .modal-body #comment").val(subjectComment);
        }
        
        $('#basicModalAddEditSubject').modal('show');
        
        $('#basicModalAddEditSubject #actionBtn').click(function(){
            subjectAbbr = $("#basicModalAddEditSubject .modal-body #abbr").val();
            subjectFullname = $("#basicModalAddEditSubject .modal-body #fullname").val();
            subjectStudygroup = $("#basicModalAddEditSubject .modal-body #studygroup").val();
            subjectCurType = $("#basicModalAddEditSubject .modal-body #curtype").val(); 
            subjectSemester = $("#basicModalAddEditSubject .modal-body #semester").val();
            subjectDiType = $("#basicModalAddEditSubject .modal-body #ditype").val();
            subjectComment = $("#basicModalAddEditSubject .modal-body #comment").val();
                
            if(eventType==="add"){
                editRowSubject("-1",subjectAbbr,subjectFullname,subjectStudygroup,subjectCurType,subjectSemester,subjectDiType,subjectComment,false);
            }   
            if(eventType==="edit"){
                editRowSubject(subjectId,subjectAbbr,subjectFullname,subjectStudygroup,subjectCurType,subjectSemester,subjectDiType,subjectComment,false);
            }
            if(eventType==="clone"){      
                editRowSubject(subjectId,subjectAbbr,subjectFullname,subjectStudygroup,subjectCurType,subjectSemester,subjectDiType,subjectComment,true);                 
            }
        });        
    }
    
    // DELETE Button from SUBJECT Action-Column    
    function subjectDeleteActionButton(){
        let subjectId = $(this).data('index');
        let subjectAbbr = $(this).data('abbr');
        let subjectFullname = $(this).data('fullname');
        let subjectSemester = $(this).data('semester');
        $("#basicModalDeleteSubject #myModalLabel").text('');
        $("#basicModalDeleteSubject #myModalLabel").append('LÖSCHEN');
        $('#basicModalDeleteSubject #deleteInfo').text("Möchten Sie wirklich " + "name: " +subjectAbbr + " - " +"Semester: " +subjectSemester + "[" + subjectFullname +  "]" +  " löschen?" );
        $('#basicModalDeleteSubject').modal('show');

        $('#basicModalDeleteSubject #actionBtn').click(function(){
            deleteRow(subjectId,"DeleteServletSUBJECT");
            
        });
    }
    /** ################  END SUBJECT Action-Column  ################### **/
});