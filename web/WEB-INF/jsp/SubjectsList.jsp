<%@page import="java.util.ArrayList"%>
<%@page import="de.hscoburg.nedev.stdplanconfig.Models.Subject"%>
<div class="table" >
    <table id="tabelle" class="display responsive nowrap compact"  width="100%">
        <thead>
            <tr>
                <th style="width: 5%">Nummer</th>
                <th style="width: 5%">Abk�rzung</th>
                <th style="width: 5%">Studiengruppe</th>
                <th style="width: 5%">Semester</th>
                <th style="width:20%">Modulname</th>
                <th style="width: 5%">CurType</th>
                <th style="width: 5%">DiType</th>
                <th style="width: 45%">Kommentar</th>
                <th style="width: 5%">Action</th>
            </tr>
        </thead>
        <tbody>
            <% 
                ArrayList<Subject> subjects = (ArrayList<Subject>)session.getAttribute("subjects");
                for(int i = 0; i< subjects.size();i++){ 
                    Subject subject = subjects.get(i);
            %>
            <tr>
                    <td><%=i+1 %></td>
                    <td><%=subject.getShortcut() %></td>
                    <td><%=subject.getStudygroupbase() %></td>
                    <td><%=subject.getStudygroupsemester() %></td>
                    <td><%=subject.getLongname() %></td>
                    <td><%=subject.getCurriculumType() %></td>
                    <td><%=subject.getDidacticType() %></td>
                    <td><%=subject.getComment()%></td>
                    <td>
                        <div class="actionButton" style="display:flex">
                            
                            <button  class="btn btn-success btn-sm" id="copyDataRowButtonSubjectModal"
                                title="Eintrag klonen" 
                                data-abbr="<%=subject.getShortcut() %>" 
                                data-studygroup="<%=subject.getStudygroupbase() %>"
                                data-semester="<%=subject.getStudygroupsemester() %>" 
                                data-fullname="<%=subject.getLongname() %>" 
                                data-curtype="<%=subject.getCurriculumType() %>" 
                                data-ditype="<%=subject.getDidacticType() %>" 
                                data-comment="<%=subject.getComment() %>" 
                                data-index="<%=i %>">
                                <i class="glyphicon glyphicon-copy"></i>
                            </button>

                            <button  class="btn btn-warning btn-sm" id="editButtonSubjectModal" data-toggle="modal" data-target="#basicModalAddEditSubject"
                                title="�ndern" 
                                data-abbr="<%=subject.getShortcut() %>" 
                                data-studygroup="<%=subject.getStudygroupbase() %>"
                                data-semester="<%=subject.getStudygroupsemester() %>" 
                                data-fullname="<%=subject.getLongname() %>" 
                                data-curtype="<%=subject.getCurriculumType() %>" 
                                data-ditype="<%=subject.getDidacticType() %>" 
                                data-comment="<%=subject.getComment() %>" 
                                data-index="<%=i %>">
                                <i class="glyphicon glyphicon-edit"></i>
                            </button>
                 
                            <!-- DELETE BUTTON MODAL -->
                            <button  class="btn btn-danger btn-sm deleteBtn " id="deleteButtonSubjectModal"
                                data-toggle="modal" data-target="#basicModalDeleteSubject"
                                title="L�schen" 
                                data-abbr="<%=subject.getShortcut() %>" 
                                data-studygroup="<%=subject.getStudygroupbase() %>"
                                data-semester="<%=subject.getStudygroupsemester() %>" 
                                data-fullname="<%=subject.getLongname() %>" 
                                data-curtype="<%=subject.getCurriculumType() %>" 
                                data-ditype="<%=subject.getDidacticType() %>" 
                                data-comment="<%=subject.getComment() %>" 
                                data-index="<%=i %>">
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            
            <% } %>
            
        </tbody>
    </table>    
</div> 