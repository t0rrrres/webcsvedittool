<%@page import="java.util.ArrayList"%>
<%@page import="de.hscoburg.nedev.stdplanconfig.Models.StudyGroup"%>
<div class="table" >
    <table id="tabelle" class="display responsive nowrap compact"  width="100%">
        <thead>
            <tr>
                <th style="width: 5%">Nummer</th>
                <th style="width: 20%">Studiengruppe</th>
                <th style="width: 10%">Semester</th>
                <th>Kommentar</th>
                <th style="width: 5%">Action</th>
            </tr>
        </thead>
        <tbody>
            <% 
                ArrayList<StudyGroup> studygroups = (ArrayList<StudyGroup>)session.getAttribute("studygroups");
                for(int i = 0; i< studygroups.size();i++){ 
                    StudyGroup sg = studygroups.get(i);
            %>
                <tr>
                    <td><%=i+1%></td>
                    <td><%=sg.getBasename() %></td>
                    <td><%=sg.getSemester() %></td>
                    <td><%=sg.getComment() %></td>
                    <td>
                        <div class="actionButton"  style="display:flex">
                            
                            <button  class="btn btn-success btn-sm" id="copyDataRowButtonStudyGroupModal" data-toggle="modal" data-target="#basicModalAddEditStudyGroup"
                                title="Eintrag klonen" 
                                data-basename="<%=sg.getBasename() %>" 
                                data-semester="<%=sg.getSemester() %>"
                                data-comment="<%=sg.getComment() %>"
                                data-index="<%=i%>">
                                <i class="glyphicon glyphicon-copy"></i>
                            </button>

                            <button  class="btn btn-warning btn-sm" id="editButtonStudyGroupModal" data-toggle="modal" data-target="#basicModalAddEditStudyGroup"
                                title="�ndern" 
                                data-basename="<%=sg.getBasename() %>" 
                                data-semester="<%=sg.getSemester() %>"
                                data-comment="<%=sg.getComment() %>"
                                data-index="<%=i%>">
                                <i class="glyphicon glyphicon-edit"></i>
                            </button>

                            <!-- DELETE BUTTON MODAL -->
                            <button  class="btn btn-danger btn-sm deleteBtn " id="deleteButtonStudyGroupModal"
                                data-toggle="modal" data-target="#basicModalDeleteStudyGroup"
                                title="L�schen" 
                                data-basename="<%=sg.getBasename() %>" 
                                data-semester="<%=sg.getSemester() %>"
                                data-comment="<%=sg.getComment() %>"
                                data-index="<%=i%>">
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            <% } %>
            
        </tbody>
    </table>    
</div> 