<%@page import="java.util.ArrayList"%>
<%@page import="de.hscoburg.nedev.stdplanconfig.Models.Room"%>
<div class="table" >
            <table id="tabelle" class="display responsive nowrap compact"  width="100%">
                <thead>
                    <tr>
                        <th style="width: 5%">Nummer</th>
                        <th style="width: 5%">Name</th>
                        <th style="width: 5%">Kapazit�t</th>
                        <th style="width: 5%">Typ</th>
                        <th style="width: 5%">Besitzer</th>
                        <th>Kommentar</th>
                        <th style="width: 5%">Action</th>
                    </tr>
                </thead>
                <tbody >
                    <% 
                        ArrayList<Room> rooms = (ArrayList<Room>)session.getAttribute("rooms");
                        for(int i = 0; i< rooms.size();i++){ 
                            Room room = rooms.get(i);
                    %>
                    <tr>
                        <td><%=i+1%></td>
                        <td><%=room.getName()%></td>
                        <td><%=room.getSize()%></td>
                        <td><%=room.getOwner()%></td>
                        <td><%=room.getType()%></td>
                        <td><%=room.getComment()%></td>
                        <td>
                            <div class="actionButton" style="display:flex">
                                
                                <button  class="btn btn-success btn-sm" id="copyDataRowButtonRoomModal"
                                    title="Eintrag klonen" 
                                    data-name="<%=room.getName()%>" 
                                    data-size="<%=room.getSize()%>"
                                    data-owner="<%=room.getOwner()%>" 
                                    data-type="<%=room.getType()%>" 
                                    data-index="<%=i%>"
                                    data-comment="<%=room.getComment()%>">
                                    <i class="glyphicon glyphicon-copy"></i>
                                </button>
                                
                                <button  class="btn btn-warning btn-sm" id="editButtonRoomModal" data-toggle="modal" data-target="#basicModalAddEditRoom"
                                    title="�ndern" 
                                    data-name="<%=room.getName()%>" 
                                    data-size="<%=room.getSize()%>"
                                    data-owner="<%=room.getOwner()%>" 
                                    data-type="<%=room.getType()%>" 
                                    data-index="<%=i%>"
                                    data-comment="<%=room.getComment()%>">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </button>
                                
                          
                                <!-- DELETE BUTTON MODAL -->
                                <button  class="btn btn-danger btn-sm deleteBtn " id="deleteButtonRoomModal" 
                                    data-toggle="modal" data-target="#basicModalDeleteRoom"
                                    title="L�schen" 
                                    data-name="<%=room.getName()%>" 
                                    data-size="<%=room.getSize()%>"
                                    data-owner="<%=room.getOwner()%>" 
                                    data-type="<%=room.getType()%>" 
                                    data-index="<%=i%>"
                                    data-comment="<%=room.getComment()%>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <% } %>          
                </tbody>
            </table>    
        </div> 