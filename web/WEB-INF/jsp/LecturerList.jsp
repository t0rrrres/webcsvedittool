<%@page import="de.hscoburg.nedev.stdplanconfig.Models.Lecturer"%>
<%@page import="java.util.ArrayList"%>

<div class="table" >
    <table id="tabelle" class="display responsive nowrap compact"  width="100%" >
        <thead>
            <tr>
                <th style="width: 5%">Nummer</th>
                <th style="width: 5%">Abk�rzung</th>
                <th style="width: 10%">Nachname</th>
                <th style="width: 10%">Vorname</th>
                <th style="width: 10%">Typ</th>
                <th>Kommentar</th>
                <th style="width: 5%">Action</th>
            </tr>
        </thead>
        <tbody>
            <% 
                ArrayList<Lecturer> dozenten = (ArrayList<Lecturer>)session.getAttribute("dozenten");
                for(int i = 0; i< dozenten.size();i++){ 
                    Lecturer lec = dozenten.get(i);
            %>
                <tr>
                    <td><%=i+1%></td>
                    <td><%=lec.getAbbr()%></td>
                    <td><%=lec.getLastName()%></td>
                    <td><%=lec.getFirstName()%></td>
                    <td><%=lec.getType()%></td>
                    <td><%=lec.getComment()%></td>
                    <td>
                        <div class="actionButton" style="display:flex">
                            <button  class="btn btn-success btn-sm" id="copyDataRowButtonLecturerModal"
                                title="Eintrag klonen" 
                                data-firstname="<%=lec.getFirstName()%>" 
                                data-lastname="<%=lec.getLastName()%>"
                                data-abbr="<%=lec.getAbbr()%>" 
                                data-type="<%=lec.getType()%>" 
                                data-comment="<%=lec.getComment()%>" 
                                data-index="<%=i%>">
                                <i class="glyphicon glyphicon-copy"></i>
                            </button>

                            <button  class="btn btn-warning btn-sm" id="editButtonLecturerModal" data-toggle="modal" data-target="#basicModalAddEditLecturer"
                                title="�ndern" 
                                data-firstname="<%=lec.getFirstName()%>" 
                                data-lastname="<%=lec.getLastName()%>"
                                data-abbr="<%=lec.getAbbr()%>" 
                                data-type="<%=lec.getType()%>" 
                                data-comment="<%=lec.getComment()%>" 
                                data-index="<%=i%>">
                                <i class="glyphicon glyphicon-edit"></i>
                            </button>
        
                            <!-- DELETE BUTTON MODAL -->
                            <button  class="btn btn-danger btn-sm deleteBtn " id="deleteButtonLecturerModal"
                                data-toggle="modal" data-target="#basicModalDeleteLecturer"
                                title="L�schen" 
                                data-firstname="<%=lec.getFirstName()%>" 
                                data-lastname="<%=lec.getLastName()%>"
                                data-abbr="<%=lec.getAbbr()%>" 
                                data-type="<%=lec.getType()%>" 
                                data-comment="<%=lec.getComment()%>" 
                                data-index="<%=i%>">
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                    
             <% } %>
            
        </tbody>
    </table>    
</div> 
                                