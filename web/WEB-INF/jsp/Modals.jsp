<%@page import="java.util.ResourceBundle" %>
<%@page import="java.util.Locale" %>
<% Locale locale = Locale.GERMAN;
    ResourceBundle bundle = ResourceBundle.getBundle("de.hscoburg.nedev.stdplanconfig.HintTextFrontend", locale);%>
<!-- MODAL DIALOG LECTURER ADD/EDIT-->
        <div class="modal fade" id="basicModalAddEditLecturer" tabindex="-1" role="dialog" aria-labelledby="basicModalAddEditLecturer" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>
              </div>
               
              <div class="modal-body"> 
                    <form  class="form-group" accept-charset="UTF-8">
                        <div class="form-group">
                            <label for="abbr">Abk�rzung</label>
                            <input type="text" class="form-control required" id="abbr" name="Abk�rzung" >
                        </div>
                        <div class="form-group">
                            <label for="last_name">Nachname</label>
                            <input type="text" class="form-control required"  id="last_name" name="Nachname" >
                        </div>
                        <div class="form-group">
                            <label for="first_name">Vorname</label>
                            <input type="text" class="form-control " id="first_name" name="Vorname" >
                        </div>                    
                        <div class="form-group">              
                            <label for="type">Typ <%=bundle.getString("DOZENTEN_TYPE_HELP_STR") %></label>
                            <input type="text" class="form-control " id="type" name="Typ" >
                        </div>
                        <div class="form-group">
                            <label for="comment">Kommentar</label>
                            <input type="text" class="form-control " id="comment" name="Kommentar">
                        </div>
                        
                    </form>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button" id='actionBtn'>Speichern</button>
                </div>
                  
              </div>
            </div>
          </div>
        </div>
        
        <!-- MODAL DIALOG LECTURER DELETE -->
        <div class="modal fade" id="basicModalDeleteLecturer" tabindex="-1" role="dialog" aria-labelledby="basicModalDeleteLecturer" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">L�SCHEN</h4>
              </div>
                
              <div class="modal-body"> 
                   
                       <div class="modal-title" id = 'deleteInfo'></div>
              </div>    
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button btn-danger" id='actionBtn'>L�schen</button>
                </div>
                  
              </div>
            </div>
        </div>
        
        
        <!-- MODAL DIALOG ROOM ADD/EDIT-->
        <div class="modal fade" id="basicModalAddEditRoom" tabindex="-1" role="dialog" aria-labelledby="basicModalAddEditRoom" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>
              </div>
               
              <div class="modal-body"> 
                    <form class="form-group" accept-charset="UTF-8">
                        <div class="form-group">
                           <label for="name">Name <%=bundle.getString("ROOM_NAME_HELP_STR") %></label>
 
                            <input type="text" class="form-control required" id="name" name="Name">
                        </div>
                        <div class="form-group">
                            <label for="size">Kapazit�t</label>
                            <input type="text" class="form-control" id="size" name="Kapazit�t">
                        </div>
                        <div class="form-group">
                            <label for="owner">Typ <%=bundle.getString("ROOM_TYPE_HELP_STR") %></label>
                            <input type="text" class="form-control required" id="type" name="Typ">
                        </div>
                        <div class="form-group">
                            <label for="type">Besitzer <%=bundle.getString("ROOM_OWNER_HELP_STR") %></label>
                            <input type="text" class="form-control " id="owner" name="Besitzer">
                        </div>
                        <div class="form-group">
                            <label for="comment">Kommentar</label>
                            <input type="text" class="form-control" id="comment" name="Kommentar">
                        </div>
                        
                    </form>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button" id='actionBtn'>Speichern</button>
                </div>
                  
              </div>
            </div>
          </div>
        </div>
        
          <!-- MODAL DIALOG ROOM DELETE -->
        <div class="modal fade" id="basicModalDeleteRoom" tabindex="-1" role="dialog" aria-labelledby="basicModalDeleteRoom" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">L�SCHEN</h4>

                    </div>  
                    <div class="modal-body"> 
                        <div class="modal-title" id = 'deleteInfo'></div>
                    </div>    

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-primary save-button btn-danger" id='actionBtn'>L�schen</button>
                    </div>     
                </div>
            </div>
        </div>
          
       <!-- MODAL DIALOG STUDYGROUP ADD/EDIT-->
        <div class="modal fade" id="basicModalAddEditStudyGroup" tabindex="-1" role="dialog" aria-labelledby="basicModalAddEditStudyGroup" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>
              </div>
               
              <div class="modal-body"> 
                    <form class="form-group" accept-charset="UTF-8">
                        <div class="form-group">
                            <label for="Basename">Studiengruppe <%=bundle.getString("STUDYGROUP_STUDEINGRUPPE_HELP_STR") %></label>
                            <input type="text" class="form-control required" id="basename" name="Name">
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester <%=bundle.getString("STUDYGROUP_SEMESTER_HELP_STR") %></label>
                            <input type="text" class="form-control required" id="stdGroupSemester" name="Semster">
                        </div> 
                        <div class="form-group">
                            <label for="semester">Kommentar</label>
                            <input type="text" class="form-control" id="stdGroupComment" name="Kommentar">
                        </div> 
                    </form>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button" id='actionBtn'>Speichern</button>
                </div>
                  
              </div>
            </div>
          </div>
        </div>
        
          <!-- MODAL DIALOG STUDYGROUP DELETE -->
        <div class="modal fade" id="basicModalDeleteStudyGroup" tabindex="-1" role="dialog" aria-labelledby="basicModalDeleteStudyGroup" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">L�SCHEN</h4>
                    </div>  
                    <div class="modal-body"> 
                        <div class="modal-title" id = 'deleteInfo'></div>
                    </div>    

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                        <button type="button" class="btn btn-primary save-button btn-danger" id='actionBtn'>L�schen</button>
                    </div>     
                </div>
            </div>
        </div>
          
          
          <!-- MODAL DIALOG SUBJECT ADD/EDIT-->
        <div class="modal fade" id="basicModalAddEditSubject" tabindex="-1" role="dialog" aria-labelledby="basicModalAddEditSubject" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>     
              </div>   
              <div class="modal-body"> 
                    <form  class="form-group" accept-charset="UTF-8">
                        <div class="form-group">
                            <label for="abbr">Abk�rzung</label>
                            <input type="text" class="form-control required"  id="abbr" name="Abk�rzung">
                        </div>
                        
                        <div class="form-group">
                            <label for="studygroup">Studiengruppe <%=bundle.getString("SUBJECT_STUDEINGRUPPE_HELP_STR") %></label>
                            <input type="text" class="form-control required" id="studygroup" name="Studeingruppe">
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester <%=bundle.getString("SUBJECT_SEMESTER_HELP_STR") %></label>
                            <input type="text" class="form-control required" id="semester" name="Semester">
                        </div>
                        <div class="form-group">
                            <label for="fullname">Modulname</label>
                            <input type="text" class="form-control required"  id="fullname" name="Modulname">
                        </div>
                        <div class="form-group">
                            <label for="curtype">CurricumTyp <%=bundle.getString("SUBJECT_CURRICULUM_TYPE_HELP_STR") %></label>
                            <input type="text" class="form-control" id="curtype" name="CurricumType">
                        </div>
                        <div class="form-group">
                            <label for="ditype">DidaktikTyp  <%=bundle.getString("SUBJECT_DIDACTIC_TYPE_HELP_STR") %></label>
                            <input type="text" class="form-control" id="ditype" name="DidaktikTyp">
                        </div>
                        <div class="form-group">
                            <label for="comment">Kommentar</label>
                            <input type="text" class="form-control" id="comment" name="Komentar">
                        </div>
                        
                    </form>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button" id='actionBtn'>Speichern</button>
                </div>
                  
              </div>
            </div>
          </div>
        </div>
        
        <!-- MODAL DIALOG SUBJECT DELETE -->
        <div class="modal fade" id="basicModalDeleteSubject" tabindex="-1" role="dialog" aria-labelledby="basicModalDeleteLecturer" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">L�SCHEN</h4>   
              </div>    
              <div class="modal-body"> 
                   
                       <div class="modal-title" id = 'deleteInfo'></div>
              </div>    
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button btn-danger" id='actionBtn'>L�schen</button>
                </div>
                  
              </div>
            </div>
        </div>
        
        
<!-- MODAL DIALOG Create New Folder-->
        <div class="modal fade" id="basicModalCreateSubFolder" tabindex="-1" role="dialog" aria-labelledby="basicModalCreateSubFolder" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>
              </div>
               
              <div class="modal-body"> 
                    <form  class="form-group" accept-charset="UTF-8">
                        <div class="form-group">
                            <label for="folderName">Ordnername <%=bundle.getString("FOLDER_NAME") %></label>
                            <input type="text" class="form-control required" id="folderName" name="Ordnername" >
                        </div>  
                    </form>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary save-button" id='actionBtn'>Erstellen</button>
                </div>
                  
              </div>
            </div>
          </div>
        </div>