<%@page import="java.io.File"%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head> 
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/login.css" type="text/css"/>
    </head>
    <body>
        <div class="login-page container">
          <div class="form">
            <form name="loginForm" method="POST" action="j_security_check">
                <input type="text" name="j_username" placeholder="username" size="20"/>
                <input type="password" size="20" placeholder="password" name="j_password"/>
                <p><input type="submit" value="Login" class="submitBtn"/></p>
            </form>                        
          </div>
        </div>  
        <script>
            sessionStorage.removeItem("reloading");
             sessionStorage.removeItem("optionValueListType");
              sessionStorage.removeItem("optionValueListYear");
        </script>
    </body>
</html>
