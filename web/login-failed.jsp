<%@page import="java.io.File"%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/login.css" type="text/css"/>
        <title>Login failed</title>
    </head>
    <body>     
         <div class="login-page">
            <div class="form">
                <div class="errorimg">
                    <img src="css/iconfinder_Cancel_27836.png" alt=""/>
                    <span>
                        <h3>Zugriff verweigert</h3>   
                        <a href="result.jsp">Zurück zur Startseite</a> 
                    </span>
                </div>
            </div>
         </div>
    </body>
</html>
