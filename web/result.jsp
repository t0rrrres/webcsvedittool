<%@page import="de.hscoburg.nedev.stdplanconfig.LogicNames"%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="de.hscoburg.nedev.stdplanconfig.VersionInfo"%>
<%@page import="de.hscoburg.nedev.stdplanconfig.FilesystemHelper" %>
<%@page import="java.io.IOException" %>

<%! 
private void errmsgListNotFound(HttpSession session, JspWriter out) throws IOException {
  String listType= (String)session.getAttribute("optionValueListType");
  String listYear= (String)session.getAttribute("optionValueListYear");   
  String errtext = "<h1 style=\"text-align: center\">Die Datei \"" + getServletContext().getInitParameter(LogicNames.getFileName(listType)) +  "\" im \"" + listYear + "\"-Ordner existiert nicht. </h1>";
  out.println(errtext);
} 
  %>
            
<%
  FilesystemHelper fh = new FilesystemHelper();
  String[] dirlist = fh.getSubdirsOfConfigPath(application);
  String logfile = getServletContext().getInitParameter(LogicNames.LOCAL_PATH_ACTION_LOGING_CONTEXT_PARAM_NAME)+
          getServletContext().getInitParameter(LogicNames.ACTIONLOG_FILENAME_CONTEXT_PARAM_NAME)+".log";
  
  String resultCreatingFolder = (String)session.getAttribute("folderCreated");
  %>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/result.css" rel="stylesheet" type="text/css"/>
        <jsp:include page="WEB-INF/jsp/_headers_offline.jsp"></jsp:include>
        
        <title><%=VersionInfo.TOOLNAME%></title>    
    </head>
    <body>
        <% if(resultCreatingFolder!=null){ %>
            <script>
                alert(`<%=resultCreatingFolder%>`);
            </script> 
        <% } %>
             
        <div class="container">
             
            <div class="nav btn-group">
                <div class="toolname">Konfigurationstool  <%=VersionInfo.TOOLNAME%> </div> 
                <div id="mylinks">
                    <select id="selectListYear">
                        <% // wss 20191111 %>
                        <% String htmltext;
                            for (int i=0; i< dirlist.length; i++) {
                            String  dirname = dirlist[i];
                            htmltext = "<option value=\""+dirname+"\">"+dirname+"</option>";
                            out.println(htmltext);
                            }
                        %>
                    </select>
                    <select  id="selectListType">
                        <option name="liste" value="lecturers" >Dozenten</option>
                        <option name="liste"  value="rooms">Räume</option>
                        <option name="liste"  value="subjects">Module</option>
                        <option name="liste"  value="studygroups">Studiengruppen</option>
                    </select>

                    <button class="btnclass" id="loadListBtn">Lade Liste</button>
                    <button class="btnclass"  id="createFolder" data-toggle="modal" data-target="#basicModalCreateSubFolder">Ordner erstellen</button>

                    <div class="navbarLogout">
                        <div class="greetings">Angemeldeter Benutzer: <%=request.getUserPrincipal().getName() %> </div>
                        <button type="submit" name="logout" id="logout" class="btnclass" value="Abmelden" >Abmelden</button>
                    </div>
                </div>
                <a href="javascript:void(0);" class="icon" id="burgerBtn">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </a>
            </div>
                <% if("lecturers".equals(session.getAttribute("optionValueListType"))){ 
                       if( !((ArrayList<Lecturer>)session.getAttribute("dozenten")==null) ) {%>   
                            <div class="btnclass">
                                <button class="addBtn" id="addButtonLecturerModal" data-id="-1" data-toggle="modal" data-target="#basicModalAddEditLecturer">Neuer Eintrag Dozent</button>
                            </div>
                            <!-- Info about ignored datarows -->
                            <% if(session.getAttribute("errorlist")!=""){ %>
                                <script>
                                    alert(`The incorect rows were ignored. For further info check <%=logfile%>`);
                                </script> 
                            <% } %>
                            <%@include file="WEB-INF/jsp/LecturerList.jsp" %>     
                       <%}else{ 
                            errmsgListNotFound(session, out);
                        }                            
                }else if("rooms".equals(session.getAttribute("optionValueListType"))){        
                        if( !((ArrayList<Room>)session.getAttribute("rooms")==null) ){ %>
                            <div class="btnclass">
                                <button class="addBtn" id="addButtonRoomModal" data-id="-1" data-toggle="modal" data-target="#basicModalAddEditRoom">Neuer Eintrag Raum</button>
                            </div>
                            <!-- Info about ignored datarows -->
                            <% if(session.getAttribute("errorlist")!=""){ %>                               
                                <script>
                                    alert(`The incorect rows were ignored. For further info check <%=logfile%>`);
                                    
                                </script> 
                            <% } %>
                            <%@ include file="WEB-INF/jsp/RoomsList.jsp"%>
                        <%}else{
                            errmsgListNotFound(session, out);
                          } 
                }else if("subjects".equals(session.getAttribute("optionValueListType"))){ 
                        if( !((ArrayList<Subject>)session.getAttribute("subjects")==null) ){ %>
                            <div class="btnclass">   
                                <button class="addBtn" id="addButtonSubjectModal" data-id="-1" data-toggle="modal" data-target="#basicModalAddEditSubject">Neuer Eintrag Studienmodul</button>
                            </div>
                            <!-- Info about ignored datarows -->
                            <% if(session.getAttribute("errorlist")!=""){ %>
                                <script> 
                                    alert(`The incorect rows were ignored. For further info check <%=logfile%>`);
                                </script> 
                            <% } %>
                            <%@ include file="WEB-INF/jsp/SubjectsList.jsp"%>
                            <%}                            
                            else{ 
                              errmsgListNotFound(session, out);
                              }  
                }else if("studygroups".equals(session.getAttribute("optionValueListType"))){ 
                        if( !((ArrayList<StudyGroup>)session.getAttribute("studygroups")==null) ){ %>
                            <div class="btnclass">
                                <button class="addBtn" id="addButtonStudyGroupModal" data-id="-1" data-toggle="modal" data-target="#basicModalAddEditStudyGroup">Neuer Eintrag Studiengruppe</button>
                            </div>
                            <!-- Info about ignored datarows -->
                            <% if(session.getAttribute("errorlist")!=""){ %>
                                <script>
                                    alert(`The incorect rows were ignored. For further info check <%=logfile%>`);
                                </script> 
                            <% } %>
                            <%@ include file="WEB-INF/jsp/StudygroupsList.jsp"%>
                            <%}else{
                            errmsgListNotFound(session, out);
                            }                     
                 }else{ %>
                    <h1 style="text-align: center">Option wählen </h1>
                <% } %> 
                <button type="submit" name="logout" id="logout" class="logout" value="Abmelden" >Abmelden</button>
                
                <div class="footer">
                    <!-- wss 20191111 -->
                <div>Hochschule Coburg (<%=VersionInfo.TOOLNAME%> Version <%=VersionInfo.VERSIONNAME%>)</div>
                </div>
        </div>
        <%@include file="WEB-INF/jsp/Modals.jsp"%>
        
        <script>
            var x = document.getElementById("mylinks");
            x.style.display = sessionStorage.getItem("burgerMenuState");
            
            let reloading = sessionStorage.getItem("reloading");
            if(!reloading){ 
                let optionValueListYear ='<%=getServletContext().getInitParameter(LogicNames.STDPLAN_DEFAULT_DROPDOWN_PATH) %>';
                $('select > option').each(function() {
                    if($(this).val()=== optionValueListYear){ 
                        $(this).attr('selected','selected');
                    } 
                });
            }  
            
        </script>
        
    </body>    
</html>
