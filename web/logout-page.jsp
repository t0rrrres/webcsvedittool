<%-- 
    Document   : login-failed
    Created on : 28.10.2019, 23:04:02
    Author     : dieter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/login.css" type="text/css"/>
    <title>Logout successfuly</title>
  </head>
  <body>
    <div class="login-page">
            <div class="form">
                <div class="errorimg">
                    <img src="" alt=""/>
                    <span>
                        <h3>Sie haben sich ausgeloggt.</h3>   
                        <a href="result.jsp">Zurück zur Startseite</a> 
                    </span>
                </div>
            </div>
         </div>
  </body>
</html>
